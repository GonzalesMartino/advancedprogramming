﻿using MathExpressionEngine;//ference of main project
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;//Added for PrintDialog
using System.Windows.Documents;//Added for FlowDocuments
using System.Windows.Input;

namespace MathInterpreterGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Program p = new Program();

        List<string> tempInput = new List<string>();//list to store input provided
        List<string> variableStored = new List<string>();//List Store variable with value
        string inputText = "";
        int count;

        public MainWindow()
        {
            InitializeComponent();
            InputBlock.Focus();
        }

        private void InputBlock_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //On key Entered pressed do the calculation and chow the result.
                if (e.Key.Equals(Key.Return) || e.Key.Equals(Key.Enter))
                {
                    inputText = InputBlock.GetLineText(0);
                    MathCalculation(inputText);
                }
                //On key up pressed show the commands added already
                if (e.Key == Key.Up)
                {
                    if (count >= 0)
                    {
                        if (count != 0)
                        {
                            InputBlock.Text = tempInput[count - 1];
                            count--;
                        }
                        else if (count == 0)
                        {
                            InputBlock.Text = tempInput[count];
                        }
                    }
                    InputBlock.CaretIndex = InputBlock.Text.Length;
                }
                if (e.Key == Key.Down)
                {
                    if (count >= 0 && count < tempInput.Count)
                    {
                        count++;
                        if (count != tempInput.Count)
                        {
                            InputBlock.Text = tempInput[count];
                        }
                    }
                    InputBlock.CaretIndex = InputBlock.Text.Length;
                }
            }
            catch (Exception ex)
            {
                OutputBlock.AppendText(ex.Message);
            }
        }

        /// <summary>
        /// Clear Button Click event
        /// It will clear both Output and Input windows 
        /// but the session and variable value will remain stored
        /// </summary>
        private void Clear_Button_Click(object sender, RoutedEventArgs e)
        {
            OutputBlock.Clear();
            InputBlock.Clear();
            InputBlock.Focus();
        }

        /// <summary>
        /// Print Button Click event
        /// Code to print Out the everything present in Output window
        /// </summary>
        private void Print_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PrintDialog pd = new PrintDialog();

                if (pd.ShowDialog().GetValueOrDefault())
                {
                    FlowDocument flowDocument = new FlowDocument();
                    flowDocument.PagePadding = new Thickness(50);
                    flowDocument.Blocks.Add(new Paragraph(new Run(OutputBlock.Text)));

                    pd.PrintDocument((((IDocumentPaginatorSource)flowDocument).DocumentPaginator), "Using Paginator");
                }
            }
            catch (Exception ex)
            {
                OutputBlock.AppendText(ex.Message);
            }
        }

        //Solve Button Click Event
        private void Solve_Button_Click(object sender, RoutedEventArgs e)
        {
            inputText = InputBlock.GetLineText(0);
            MathCalculation(inputText);
        }

        private void MathCalculation(string input)
        {
            try
            {
                tempInput.Add(input);//Added the input to list
                string output = p.operation(input);
                OutputBlock.AppendText(">>" + " " + input);
                OutputBlock.AppendText(Environment.NewLine);
                OutputBlock.AppendText(output);

                #region Variable session
                /// <summary>
                /// This part have the code to show variable and its value in Variable Session
                /// This part also include the code to show change in variable using input block
                /// and refelect the new value in Variable session
                /// </summary>
                if (output.Contains(":"))
                {
                    if (output.Contains(":="))
                    {
                        int listCount = variableStored.Count;
                        if (listCount == 0)
                        {
                            variableStored.Add(output);
                        }

                        string[] varValue = output.Split('=');
                        int listSize = listCount - 1;
                        for (int i = 0; i < listCount; i++)
                        {
                            string[] compare = variableStored[i].Split('=');
                            if (variableStored[i].Equals(varValue[0] + "=" + compare[1]))
                            {
                                variableStored[i] = variableStored[i].Replace(variableStored[i], output);
                                break;
                            }
                            else if (i == listSize)
                            {
                                variableStored.Add(output);
                            }
                        }

                        VariableSession.Clear();
                        int newListCount = variableStored.Count;
                        for (int i = 0; i < newListCount; i++)
                        {
                            VariableSession.AppendText(variableStored[i]);
                            VariableSession.AppendText(Environment.NewLine);
                        }


                    }
                    else
                    {
                        int listCount = variableStored.Count;
                        string[] varValue = output.Split(':');
                        for (int i = 0; i < listCount; i++)
                        {
                            string[] compare = variableStored[i].Split('=');
                            if (variableStored[i].Equals(varValue[0] + ":=" + compare[1]))
                            {
                                variableStored.RemoveAt(i);
                                break;
                            }
                        }

                        VariableSession.Clear();
                        int newListCount = variableStored.Count;
                        for (int i = 0; i < newListCount; i++)
                        {
                            VariableSession.AppendText(variableStored[i]);
                            VariableSession.AppendText(Environment.NewLine);
                        }
                    }


                    ///<summary>
                    /// Part below to add variable names to combobox
                    ///</summary>
                    SelectVariable.Items.Clear();
                    int colonIndex;
                    foreach (string pair in variableStored)
                    {
                        colonIndex = pair.IndexOf(':');
                        SelectVariable.Items.Add(pair.Substring(0, colonIndex));
                    }
                }

                #endregion
                OutputBlock.AppendText(Environment.NewLine);
                OutputBlock.ScrollToEnd();
                InputBlock.Clear();
                InputBlock.Focus();
                count = tempInput.Count;
            }
            catch (Exception ex)
            {
                OutputBlock.AppendText(ex.Message);
                OutputBlock.AppendText(Environment.NewLine);
            }
        }

        ///<summary>
        /// Button to plot graph only when the input contain plot keyword else it will throw error
        /// </summary>
        private void Plot_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                inputText = InputBlock.GetLineText(0);
                if (inputText.Contains("plot"))
                {
                    MathCalculation(inputText);
                }
                else if (Regex.IsMatch(inputText, "[A-Z]"))
                {
                    MathCalculation("plot(" + inputText.Trim() + ")");
                }
                else
                {
                    throw new MathExpressionEngine.utilities.exceptions.NoPolynomialFoundException(inputText);
                }
            }
            catch (Exception ex)
            {
                OutputBlock.AppendText(ex.Message);
            }
        }

        //************** Edit Variable funtionality Start here  ******************//
        private void PreviewTextInput(Object sender, TextCompositionEventArgs e)
        {
            e.Handled = new Regex("[^0-9.-]+").IsMatch(e.Text);
        }

        private void Edit_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string editVariablestr;
                if (SelectVariable.SelectedIndex != -1)
                {
                    if (EditVariableValue.Text.Length != 0)
                    {
                        editVariablestr = string.Format("{0}:={1}", SelectVariable.SelectedValue, EditVariableValue.Text.Trim());
                        MathCalculation(editVariablestr);
                    }
                    else
                    {
                        OutputBlock.AppendText("Please enter value to edit variable");
                        OutputBlock.AppendText(Environment.NewLine);
                    }
                }
                else
                {
                    OutputBlock.AppendText("Please select Variable to edit");
                    OutputBlock.AppendText(Environment.NewLine);
                }

                EditVariableValue.Text = "";
            }
            catch (Exception ex)
            {
                OutputBlock.AppendText(ex.Message);
                OutputBlock.AppendText(Environment.NewLine);
            }
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            string deleteVariableName = string.Empty;
            try
            {
                deleteVariableName = Convert.ToString(SelectVariable.SelectedValue);
                Interpreter.GLOBAL_VAR.Remove(deleteVariableName);

                int listSize = variableStored.Count;
                for (int i = 0; i <= listSize; i++)
                {
                    string[] compare = variableStored[i].Split('=');
                    if (variableStored[i].Equals(deleteVariableName + ":=" + compare[1]))
                    {
                        variableStored.RemoveAt(i);
                        break;
                    }
                }

                VariableSession.Clear();
                int newListCount = variableStored.Count;
                for (int i = 0; i < newListCount; i++)
                {
                    VariableSession.AppendText(variableStored[i]);
                    VariableSession.AppendText(Environment.NewLine);
                }

                SelectVariable.Items.Clear();
                int colonIndex;
                foreach (string pair in variableStored)
                {
                    colonIndex = pair.IndexOf(':');
                    SelectVariable.Items.Add(pair.Substring(0, colonIndex));
                }
            }
            catch (Exception ex)
            {
                throw new MathExpressionEngine.utilities.exceptions.UnspecifiedVariableFound(deleteVariableName);
            }
        }
        //**************  Edit Variable funtionality End here ******************//
    }
}
