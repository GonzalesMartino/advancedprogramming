﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathExpressionEngine;

namespace InterpreterTest
{
    [TestClass]
    public class InterpreterTests
    {
        [TestMethod]
        public void expr_addTwoSingleDigitInteger_Correct()
        {
            var inter = new Interpreter("3+5");

            double result = inter.expr();

            Assert.AreEqual<double>(result,8);
        }
   

        [TestMethod]
        public void expr_subtractTwoSingleDigitInteger_Correct()
        {
            var inter = new Interpreter("5-3");

            double result = inter.expr();

            Assert.AreEqual<double>(result, 2);
        }

        [TestMethod]
        public void expr_multiplyTwoSingleDigitInteger_Correct()
        {
            var inter = new Interpreter("3*5");

            double result = inter.expr();

            Assert.AreEqual<double>(result, 15);
        }

        [TestMethod]
        public void expr_divideTwoSingleDigitInteger_Correct()
        {
            var inter = new Interpreter("6/3");

            double result = inter.expr();

            Assert.AreEqual<double>(result, 2);
        }

        [TestMethod]
        public void expr_ignoreWhiteSpaces()
        {
            var inter = new Interpreter("            3            * 2             ");

            double result = inter.expr();

            Assert.AreEqual<double>(result, 6);
        }

        [TestMethod]
        public void expr_addTwoDouble_Correct()
        {
            var inter = new Interpreter("6.384 + 11.348");

            double result = inter.expr();

            Assert.AreEqual<double>(result, 17.732);
        }

        [TestMethod]
        public void expr_subtractTwoDouble_Correct()
        {
            var inter = new Interpreter("45.3784-67.248");

            double result = inter.expr();

            // Needed to put -21.869600000000005 given rounding 
            Assert.AreEqual<double>(result, (-21.869600000000005));
        }

        [TestMethod]
        public void expr_multiplyTwoDouble_Correct()
        {
            var inter = new Interpreter("4.85*7.33");

            double result = inter.expr();

            Assert.AreEqual<double>(result, 35.5505);
        }

        [TestMethod]
        public void expr_divideTwoDouble_Correct()
        {
            var inter = new Interpreter("159.3/10.0");

            double result = inter.expr();

            Assert.AreEqual<double>(result, 15.930000000000002);
        }





    }
}
