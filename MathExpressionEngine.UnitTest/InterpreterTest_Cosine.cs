﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathExpressionEngine.UnitTest
{
    [TestClass]
    public class InterpreterTest_Cosine
    {
        [TestMethod]
        public void Cose()
        {
            // cos(0) = 1
            string expr = "cos(0)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void Cose2()
        {
            // cos(180) = -1
            string expr = "cos(180)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -1);
        }

        [TestMethod]
        public void Cose3()
        {
            // cos(30) = 0.86602540378443871
            string expr = "cos(30)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0.86602540378443871);
        }

        [TestMethod]
        public void Cose4()
        {
            // cos(150) = -0.86602540378443871
            string expr = "cos(150)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -0.86602540378443871);
        }

        [TestMethod]
        public void Cose5()
        {
            // cos(45) = 0.70710678118654752440084436210485
            string expr = "cos(45)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0.70710678118654752440084436210485);
        }

        [TestMethod]
        public void Cose6()
        {
            // cos(135) = -0.70710678118654746
            string expr = "cos(135)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -0.70710678118654746);
        }

        [TestMethod]
        public void Cose7()
        {
            // cos(60) = 0.5
            string expr = "cos(60)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            // I test it as a string since it would evaluate to something as
            // 0.49999999999999994 but when it print it it prints the correct value 0.5
            Assert.AreEqual(result+"", "0.5");
        }

        [TestMethod]
        public void Cose8()
        {
            // cos(120) = -0.5
            string expr = "cos(120)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            // I test it as a string since it would evaluate to something as
            // 0.49999999999999994 but when it print it it prints the correct value 0.5
            Assert.AreEqual(result + "", "-0.5");
        }

        [TestMethod]
        public void Cose9()
        {
            // cos(90) = 0
            string expr = "cos(90)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void Cose10()
        {
            // cos(30.5) = 0.86162916044152582
            string expr = "cos(30.5)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0.86162916044152582);
        }

        [TestMethod]
        public void Cose11()
        {
            // cos(0.001) = 0.99999999984769129011051202417815
            string expr = "cos(0.001)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0.99999999984769129011051202417815);
        }
    }
}
