﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathExpressionEngine.UnitTest
{
    [TestClass]
    public class InterpreterTest_Sine
    {
        [TestMethod]
        public void Sine()
        {
            // sin(0) = 0
            string expr = "sin(0)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void Sine2()
        {
            // sin(30) = 0.5
            string expr = "sin(30)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            // I test it as a string since it would evaluate to something as
            // 0.49999999999999994 but when it print it it prints the correct value 0.5
            Assert.AreEqual(result+"", "0.5");
        }

        [TestMethod]
        public void Sine3()
        {
            // sin(-30) = -0.5
            string expr = "sin(-30)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result + "", "-0.5");
        }

        [TestMethod]
        public void Sine4()
        {
            // sin(45) = 0.70710678118654746
            string expr = "sin(45)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0.70710678118654746);
        }

        [TestMethod]
        public void Sine5()
        {
            // sin(-45) = -0.70710678118654746
            string expr = "sin(-45)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -0.70710678118654746);
        }

        [TestMethod]
        public void Sine7()
        {
            // sin(60) = 0.86602540378443864676372317075294
            string expr = "sin(60)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0.86602540378443864676372317075294);
        }

        [TestMethod]
        public void Sine8()
        {
            // sin(-60) = -0.86602540378443864676372317075294
            string expr = "sin(-60)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -0.86602540378443864676372317075294);
        }

        [TestMethod]
        public void Sine10()
        {
            // sin(90) = 1
            string expr = "sin(90)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 1);
        }


        [TestMethod]
        public void Sine11()
        {
            // sin(-90) = -1
            string expr = "sin(-90)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -1);
        }

        [TestMethod]
        public void Sine12()
        {
            // sin(30.5) = 0.50753836296070409
            string expr = "sin(30.5)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0.50753836296070409);
        }

        [TestMethod]
        public void Sine13()
        {
            // sin(0.001) = 1.7453292519057202E-05
            string expr = "sin(0.001)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 1.7453292519057202E-05);
        }
    }
}
