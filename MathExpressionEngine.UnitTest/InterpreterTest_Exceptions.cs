﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;

namespace MathExpressionEngine.UnitTest
{
    [TestClass]
    public class InterpreterTest_Exceptions
    {
        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.CommandNotFoundException))]
        public void Exception_CommandNotFound()
        {
            // 5*root(8) -> root is not a defined command so 
            //              it should throw an exception 
            string expr = "5*root(8)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.DivideByZeroException))]
        public void Exception_DivideByZero()
        {
            // -22/0 -> invalid
            string expr = "-22/0";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.DivideByZeroException))]
        public void Exception_DivideByZero2()
        {
            // tan(90) = sin(90)/cos(90) -> cos(90) = 0 
            string expr = "tan(90)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.ExpectedTokenMismatchException))]
        public void Exception_ExpectedTokenMismatch()
        {
            // (9+) -> missing the second ID token for the binary operation 
            string expr = "(9+)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.InvalidArgumentFactorialException))]
        public void Exception_InvalidArgumentFactorial()
        {
            // 0.5!-> cannot calculate factorial of fraction
            string expr = "0.5!";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.InvalidArgumentFactorialException))]
        public void Exception_InvalidArgumentFactorial2()
        {
            // (-5)!-> cannot calculate factorial of negative number
            string expr = "(-5)!";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.InvalidArgumentForPlotException))]
        public void Exception_InvalidArgumentForPlotException()
        {
            // plot Y:3 will fail since it is not a function 
            string expr = "Y:3";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();

            expr = "plot(Y)";

            l = new Lexer(expr);

            p = new Parser(l);

            i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.InvalidRootArgumentException))]
        public void Exception_InvalidRootArgument()
        {
            // nthroot(-9,2) -> cannot calculate the square root of a negative number 
            string expr = "nthroot(-9,2)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.InvalidRootArgumentException))]
        public void Exception_InvalidRootArgument2()
        {
            // nthroot(9,0) -> cannot calculate the 0 root of any number 
            string expr = "nthroot(9,0)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.InvalidRootArgumentException))]
        public void Exception_InvalidRootArgument3()
        {
            // nthroot(0,-3) -> cannot calculate a negative root of 0
            string expr = "nthroot(0,-3)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.InvalidStartEndPointForPlotException))]
        public void Exception_InvalidStartEndPointForPlot()
        {
            // Y:3*X specify the function to plot
            string expr = "Y:3*X";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();

            // try to plot it form 10 to -10
            expr = "plot(Y,10,-10)";

            l = new Lexer(expr);

            p = new Parser(l);

            i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.ModByZeroException))]
        public void Exception_ModByZero()
        {
            // 1756%0 = 0
            string expr = "1756%0";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();

        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.NoInputEnteredException))]
        public void Exception_NoInputEntered()
        {

            string expr = "  ";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.NoPolynomialFoundException))]
        public void Exception_NoPolynomialFound()
        {
            // Try to plot a polynomial which was not previously defined
            string expr = "plot(T)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.NoPolynomialFoundException))]
        public void Exception_NoPolynomialFound2()
        {
            // Try to find the minmax of a polynomial not previously defined
            string expr = "minmax(T)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.NoPolynomialVariableFoundException))]
        public void Exception_NoPolynomialVariableFound()
        {
            // Y:3+2*x -> define polynomial with incorrect variable x -> must be uppercase
            string expr = "Y:3+2*x";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();

            expr = "plot(Y)";

            l = new Lexer(expr);

            p = new Parser(l);

            i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.UnmatchParenthesesException))]
        public void Exception_UnmatchParentheses()
        {
            // 1(+1 -> Exception
            string expr = "1(+1";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.UnspecifiedTokenFoundException))]
        public void Exception_UnspecifiedTokenFound()
        {
            // 2@6 -> @ is undefined token
            string expr = "2@6";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.UnspecifiedVariableFound))]
        public void Exception_UnspecifiedVariableFound()
        {
            // A+3
            string expr = "A+3";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.WrongPolyExpressionFormatForMinMax))]
        public void Exception_WrongPolyExpressionFormatForMinMax()
        {
            // Y:3*X^2+2*X-1 -> should be Y:3*X^2+2*X^1-1
            string expr = "Y:3*X^2+2*X-1";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();

            expr = "minmax(Y)";

            l = new Lexer(expr);

            p = new Parser(l);

            i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.WrongUseOfSeparatorException))]
        public void Exception_WrongUseOfSeparator()
        {
            // 4,+2 
            string expr = "4,+2";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            i.interpret();
        }

        [TestMethod]
        [ExpectedException(typeof(utilities.exceptions.VariableAlreadyDefinedExceptions))]
        public void Exception_VariableAlreadyDefined()
        {
            // define variable A:=3 
            string expr = "A:=3";
            Lexer l = new Lexer(expr);
            Parser p = new Parser(l);
            Interpreter i = new Interpreter(p);
            i.interpret();

            // Define poly to plot with already defined variable
            expr = "Z:3*A";
            l = new Lexer(expr);
            p = new Parser(l);
            i = new Interpreter(p);
            i.interpret();

            // Try to plot it
            expr = "plot(Z)";
            l = new Lexer(expr);
            p = new Parser(l);
            i = new Interpreter(p);
            i.interpret();
        }
    }
}
