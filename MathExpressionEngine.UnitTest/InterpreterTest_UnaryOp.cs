﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathExpressionEngine.UnitTest
{
    [TestClass]
    public class InterpreterTest_UnaryOp
    {
        [TestMethod]
        public void UnaryOp()
        {
            // 1--1 = 2
            string expr = "1--1";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 2);
        }

        [TestMethod]
        public void UnaryOp2()
        {
            // -1+3
            string expr = "-1+3";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 2);
        }

        [TestMethod]
        public void UnaryOp3()
        {
            // -(3+-5) = 2
            string expr = "-(3+-5)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 2);
        }

        [TestMethod]
        public void UnaryOp4()
        {
            // ---------1 = -1
            string expr = "---------1";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -1);
        }
    }
}
