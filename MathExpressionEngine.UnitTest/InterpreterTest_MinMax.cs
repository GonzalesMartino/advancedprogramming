﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;

namespace MathExpressionEngine.UnitTest
{
    [TestClass]
    public class InterpreterTest_MinMax
    {
        [TestMethod]
        public void MinMax()
        {
            // Test if get right resutl by finding min or max of 2*X^2 + 4*X^1 + 6
            string expr = "Y:2*X^2 + 4*X^1 + 6";
            Lexer l = new Lexer(expr);
            Parser p = new Parser(l);
            Interpreter i = new Interpreter(p);
            i.interpret();

            expr = "minmax(Y)";
            l = new Lexer(expr);
            p = new Parser(l);
            i = new Interpreter(p);

            var result = i.interpret();

            StringBuilder correctResult = new StringBuilder();
            correctResult.Append("Degree of the polynomial :" + 2 + "\n");
            correctResult.Append("Polynomial Coefficient : [ 2 4 6 ]\n");
            correctResult.Append("First Derivative Coefficients : [ 4 4 ]\n");
            correctResult.Append("Second Derivative Coefficients : [ 4 ]\n");
            correctResult.Append("There is a minimum at point [ " + -1 + "," + 4 + "]");

            Assert.AreEqual(result, correctResult.ToString());
        }

        [TestMethod]
        public void MinMax2()
        {
            // Test if get right resutl by finding min or max of -2*X^2 + 4*X^1 + 6
            string expr = "Y:-2*X^2 + 4*X^1 + 6";
            Lexer l = new Lexer(expr);
            Parser p = new Parser(l);
            Interpreter i = new Interpreter(p);
            i.interpret();

            expr = "minmax(Y)";
            l = new Lexer(expr);
            p = new Parser(l);
            i = new Interpreter(p);

            var result = i.interpret();

            StringBuilder correctResult = new StringBuilder();
            correctResult.Append("Degree of the polynomial :2\n");
            correctResult.Append("Polynomial Coefficient : [ -2 4 6 ]\n");
            correctResult.Append("First Derivative Coefficients : [ -4 4 ]\n");
            correctResult.Append("Second Derivative Coefficients : [ -4 ]\n");
            correctResult.Append("There is a maximum at point [ " + 1 + "," + 8 + "]");

            Assert.AreEqual(result, correctResult.ToString());
        }

        [TestMethod]
        public void MinMax3()
        {
            // Test if get right resutl by finding min or max of -2*X^1 + 6
            string expr = "Y:-2*X^1 + 6";
            Lexer l = new Lexer(expr);
            Parser p = new Parser(l);
            Interpreter i = new Interpreter(p);
            i.interpret();

            expr = "minmax(Y)";
            l = new Lexer(expr);
            p = new Parser(l);
            i = new Interpreter(p);

            var result = i.interpret();

            string correctResult = "Cannot find min or max of a linear degree polynomial : -2*X^1+6";

            Assert.AreEqual(result, correctResult);
        }

        [TestMethod]
        public void MinMax4()
        {
            // Test if get right resutl by finding min or max of -2*X^1 - 3*X^2 + 5*X^3 + 6
            string expr = "Y:-2*X^1 - 3*X^2 + 5*X^3 + 6";
            Lexer l = new Lexer(expr);
            Parser p = new Parser(l);
            Interpreter i = new Interpreter(p);
            i.interpret();

            expr = "minmax(Y)";
            l = new Lexer(expr);
            p = new Parser(l);
            i = new Interpreter(p);

            var result = i.interpret();

            string correctResult = "Not implemented yet for polynomials greater of the second degree";

            Assert.AreEqual(result, correctResult);
        }
    }
}
