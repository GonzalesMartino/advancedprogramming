﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathExpressionEngine.UnitTest
{
    [TestClass]
    public class InterpreterTest_Tangent
    {
        [TestMethod]
        public void Tan()
        {
            // tan(0) = 0
            string expr = "tan(0)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void Tan2()
        {
            // tan(180) = 0
            string expr = "tan(180)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void Tan3()
        {
            // tan(30) = 0.57735026918962562
            string expr = "tan(30)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0.57735026918962562);
        }

        [TestMethod]
        public void Tan4()
        {
            // tan(150) = -0.57735026918962562
            string expr = "tan(150)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -0.57735026918962562);
        }

        [TestMethod]
        public void Tan5()
        {
            // tan(45) = 1
            string expr = "tan(45)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0.99999999999999989);
        }

        [TestMethod]
        public void Tan6()
        {
            // tan(135) = -1
            string expr = "tan(135)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -1.0000000000000002);
        }

        [TestMethod]
        public void Tan7()
        {
            // tan(60) = 1.7320508075688768
            string expr = "tan(60)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 1.7320508075688768);
        }

        [TestMethod]
        public void Tan8()
        {
            // tan(120) = -1.7320508075688781
            string expr = "tan(120)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -1.7320508075688781);
        }

        [TestMethod]
        public void Tan9()
        {
            // tan(89.99) = 5729.5778931216482
            string expr = "tan(89.99)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 5729.5778931216482);
        }

        [TestMethod]
        public void Tan10()
        {
            // tan(30.5) = 0.589045016420551
            string expr = "tan(30.5)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0.589045016420551);
        }

        [TestMethod]
        public void Tan11()
        {
            // tan(0.001) = 1.7453292521715488080855439976596e-5
            string expr = "tan(0.001)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 1.7453292521715488080855439976596e-5);
        }
    }
}
