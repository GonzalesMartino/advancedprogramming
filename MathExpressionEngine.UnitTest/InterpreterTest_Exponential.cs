﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathExpressionEngine.UnitTest
{
    [TestClass]
    public class InterpreterTest_Exponential
    {
        [TestMethod]
        public void Exponential1()
        {
            // 11^7= 19487171
            string expr = "11^7";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 19487171);
        }

        [TestMethod]
        public void Exponential2()
        {
            // 11^-7= 5.1315811823070675574202125080136e-8
            string expr = "11^-7";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 5.1315811823070675574202125080136e-8);
        }

        [TestMethod]
        public void Exponential3()
        {
            // 11^0= 1
            string expr = "11^0";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result,1);
        }

        [TestMethod]
        public void Exponential4()
        {
            // 11.032^4 = 14812.112866840576
            string expr = "11.032^4";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 14812.112866840576);
        }


        [TestMethod]
        public void Exponential5()
        {
            // 2^2.2 = 4.5947934199881400271945077871117
            string expr = "2^2.2";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 4.5947934199881400271945077871117);
        }
    }
}
