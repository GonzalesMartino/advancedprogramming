﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathExpressionEngine.UnitTest
{
    [TestClass]
    public class InterpreterTest_Nthroot
    {
        [TestMethod]
        public void nthroot()
        {
            // nthroot(158,2) = 12.569805089976534715702558653614
            string expr = "nthroot(158,2)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 12.569805089976534715702558653614);
        }

        [TestMethod]
        public void nthroot2()
        {
            // nthroot(158,3) = 5.4061201757502247670180371257702
            string expr = "nthroot(158,3)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 5.4061201757502247670180371257702);
        }

        [TestMethod]
        public void nthroot3()
        {
            // nthroot(78.25,2) = 8.8459030064770662733950175855449
            string expr = "nthroot(78.25,2)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 8.8459030064770662733950175855449);
        }

        [TestMethod]
        public void nthroot4()
        {
            // nthroot(-158,3) = -5.4061201757502247670180371257702
            string expr = "nthroot(-158,3)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -5.4061201757502247670180371257702);
        }

        [TestMethod]
        public void nthroot5()
        {
            // nthroot(0,3) = 0
            string expr = "nthroot(0,3)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void nthroot6()
        {
            // nthroot(158,1) = 158
            string expr = "nthroot(158,1)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 158);
        }
    }
}
