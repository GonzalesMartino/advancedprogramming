﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathExpressionEngine;

namespace MathExpressionEngine.UnitTest
{
    [TestClass]
    public class InterpreterTest_BinaryOperation
    {
        [TestMethod]
        public void BinaryOperation_Addition()
        {
            // 22+5 = 27
            string expr = "22+5";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 27);
        }

        [TestMethod]
        public void BinaryOperation_Addition2()
        {
            // 0.0000142+0.0000001203 = 1.43203E-05
            string expr = "0.0000142+0.0000001203";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result + "", "1.43203E-05");
        }

        [TestMethod]
        public void BinaryOperation_Addition3()
        {
            // -9+5 = -4
            string expr = "-9+5";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -4);
        }

        [TestMethod]
        public void BinaryOperation_Subtraction()
        {
            // 22-5 = 17
            string expr = "22-5";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result,17);
        }

        [TestMethod]
        public void BinaryOperation_Subtraction2()
        {
            // 0.0000142-0.0000001203 = 1.40797E-05
            string expr = "0.0000142-0.0000001203";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result + "", "1.40797E-05");
        }

        [TestMethod]
        public void BinaryOperation_Subtraction3()
        {
            // -9-5 = -14
            string expr = "-9-5";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -14);
        }


        [TestMethod]
        public void BinaryOperation_Multiplication()
        {
            // 5*3 = 15
            string expr = "5*3";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void BinaryOperation_Multiplication2()
        {
            // -5*3 = -15
            string expr = "-5*3";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -15);
        }

        [TestMethod]
        public void BinaryOperation_Multiplication3()
        {
            // -5*-3 = 15
            string expr = "-5*-3";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void BinaryOperation_Multiplication4()
        {
            // 0.0000142*0.0000001203 = 1.70826E-12
            string expr = "0.0000142*0.0000001203";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result+"", "1.70826E-12");
        }

        [TestMethod]
        public void BinaryOperation_Multiplication5()
        {
            // 23*0 = 0
            string expr = "23*0";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result,0);
        }

        [TestMethod]
        public void BinaryOperation_Division()
        {
            // 115/5 = 23
            string expr = "115/5";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 23);
        }

        [TestMethod]
        public void BinaryOperation_Division2()
        {
            // 0.0000142/0.0000001203 = 118.038237738986
         string expr = " 0.0000142/0.0000001203";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 118.03823773898586);
        }

        [TestMethod]
        public void BinaryOperation_Division3()
        {
            // 22/5 = 4.4
            string expr = "22/5";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 4.4);
        }


        [TestMethod]
        public void BinaryOperation_Divisio4()
        {
            // -22/5 = -4.4
            string expr = "-22/5";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -4.4);
        }


        [TestMethod]
        public void BinaryOperation_Division5()
        {
            // -22/-5 = 4.4
            string expr = "-22/-5";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 4.4);
        }

        [TestMethod]
        public void BinaryOperation_Modulo()
        {
            // 1756%7 = 6
            string expr = "1756%7";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 6);
        }

        [TestMethod]
        public void BinaryOperation_Modulo2()
        {
            // 1756%2 = 0
            string expr = "1756%2";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void BinaryOperation_Modulo3()
        {
            // 1756%1 = 0
            string expr = "1756%1";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void BinaryOperation_Modulo5()
        {
            // -1756%7 = -6
            string expr = "-1756%7";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -6);
        }

        [TestMethod]
        public void BinaryOperation_Modulo6()
        {
            // 1756%-7 = 6
            string expr = "1756%-7";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 6);
        }

        [TestMethod]
        public void BinaryOperation_Modulo7()
        {
            // -1756%-7 = -6
            string expr = "-1756%-7";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -6);
        }

        [TestMethod]
        public void BinaryOperation_ConcatenationOfBinaryOp()
        {
            // 5/2*7+1-3*8+4/9-3+2*4/7-3 = -9.9126984126984126984126984126984
            string expr = "5/2*7+1-3*8+4/9-3+2*4/7-3";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -9.9126984126984126984126984126984);
        }

        [TestMethod]
        public void BinaryOperation_ConcatenationOfBinaryOp_withBrackets()
        {
            // ((9)+(3*2-4/(2-4+6))*3-2)-(((3*2)-4+3*2)/2)+((((1))+1)+1) = 21
            string expr = "((9)+(3*2-4/(2-4+6))*3-2)-(((3*2)-4+3*2)/2)+((((1))+1)+1)";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 21);
        }
    }
}
