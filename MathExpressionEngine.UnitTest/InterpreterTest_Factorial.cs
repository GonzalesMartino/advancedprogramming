﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathExpressionEngine.UnitTest
{
    [TestClass]
    public class InterpreterTest_Factorial
    {
        [TestMethod]
        public void factorial()
        {
            // 8! = 40320
            string expr = "8!";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, 40320);
        }

        [TestMethod]
        public void factorial2()
        {
            // -8! = -40320
            string expr = "-8!";

            Lexer l = new Lexer(expr);

            Parser p = new Parser(l);

            Interpreter i = new Interpreter(p);

            var result = i.interpret();

            Assert.AreEqual(result, -40320);
        }
    }
}
