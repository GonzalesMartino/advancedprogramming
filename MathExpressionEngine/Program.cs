﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MathExpressionEngine
{
    public class Program
    {
        public string operation(string abc)
        {
            string input = string.Empty;
            string output = string.Empty;
            try
            {
                input = abc;

                // Initialise a Lexer object passing the input
                Lexer l = new Lexer(input);
                // Initialising a Parser object passing the Lexer
                Parser p = new Parser(l);
                // Initialising the Interpreter object passing the Parser
                Interpreter i = new Interpreter(p);
                // Use the interpreter to interpret the input from the user and display it to the console
                var result = i.interpret();
                //Console.WriteLine(result);
                output = Convert.ToString(result);
            }
            catch (Exception e)
            {
                //TextWriter errorWriter = Console.Error;
                output = e.Message;
                //Environment.Exit(0);
            }


            return output;
        }
        
        public static void Main(string[] args)
        {
            
        }
    }
}
