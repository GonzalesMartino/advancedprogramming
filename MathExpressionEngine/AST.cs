﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine
{
    /// <summary>
    /// Abstract Syntax Tree class in which we are going to model all the node 
    /// that are allowed in our interpreter
    /// </summary>
    public class AST
    {
        /// <summary>
        /// Empty class so that we can use it to model nodes in our Abstract Syntax Tree.
        /// So that every node that we are going to model are going to inherit from this class 
        /// so that we can easily implement and use different node type
        /// </summary>
        public class ASTNode { }

        /// <summary>
        /// Node of the abstract syntax tree to model a binary operation.
        /// A binary operation is made of a left and right operand and an operator between them 
        /// </summary>
        public class BinaryOp : ASTNode
        {
           
            // The left and right operator are going to be modelled as ASTNode so that 
            // we can visit them and evaluate them separately and in the right order
            private ASTNode leftOperand;
            private ASTNode rightOperand;
            // The token representing the operator 
            private Token oper;

            /// <summary>
            /// Constructor to create a BinaryOp object
            /// </summary>
            /// <param name="left">The left operand</param>
            /// <param name="op">The operator of this binary operation</param>
            /// <param name="right">The right operand</param>
            public BinaryOp(ASTNode left, Token op, ASTNode right)
            {
                this.leftOperand = left;
                this.oper = op;
                this.rightOperand = right;
            }

            /// <summary>
            /// Method to return the left operand of this Binary operation
            /// </summary>
            /// <returns>The ASTNode representing the left operand</returns>
            public ASTNode getLeftOperand()
            {
                return this.leftOperand;
            }

            /// <summary>
            /// Method to return the right operand of this Binary operation 
            /// </summary>
            /// <returns>The ASTNode representing the right operator</returns>
            public ASTNode getRightOperand()
            {
                return this.rightOperand;
            }

            /// <summary>
            /// Return the operator of this Binary operation
            /// </summary>
            /// <returns>Return the Token representing the operator</returns>
            public Token getOperator()
            {
                return this.oper;
            }
        }

        /// <summary>
        /// Node of the abstract syntax tree to model a Number node.
        /// A Number node is simple a node that represent a number.
        /// </summary>
        public class Number : ASTNode
        {
            // Token representing that number
            private Token number;

            /// <summary>
            /// Constructor to create a Number Node
            /// </summary>
            /// <param name="n"></param>
            public Number(Token n)
            {
                this.number = n;
            }

            /// <summary>
            /// Method to get Token representing the number
            /// </summary>
            /// <returns>The Token representing the number</returns>
            public Token getNumber()
            {
                return this.number;
            }
        }

        /// <summary>
        /// Node representing an exponential operation which is made of a base and an exponent
        /// </summary>
        public class Exp : ASTNode
        {
            private ASTNode baseExpr;
            private ASTNode exp;

            /// <summary>
            /// Constructor to create an exponential node
            /// </summary>
            /// <param name="baseExpr">The base of the exponential</param>
            /// <param name="expExpr">The exponent</param>
            public Exp(ASTNode baseExpr, ASTNode expExpr)
            {
                this.baseExpr = baseExpr;
                this.exp = expExpr;
            }

            /// <summary>
            /// Method to get the base of the exponential operation
            /// </summary>
            /// <returns>The base of the exponential</returns>
            public ASTNode getBaseExpr()
            {
                return this.baseExpr;
            }

            /// <summary>
            /// Method to get the exponent
            /// </summary>
            /// <returns>The exponent</returns>
            public ASTNode getExp()
            {
                return this.exp;
            }
        }

        /// <summary>
        /// Node of the abstract syntax tree to model a Unary operation.
        /// This node is used to medel what are called a unary + and unary -
        /// </summary>
        public class UnaryOp : ASTNode
        {
            // Token that represent the operator
            private Token oper;
            // The operator is going to be represented by an ASTNode since could 
            // evaluate to an expression 
            private ASTNode expr;

            /// <summary>
            /// Constructor to create a UnaryOp object
            /// </summary>
            /// <param name="t">The token representing this unary operator</param>
            /// <param name="node">The ASTNode representing the operand</param>
            public UnaryOp(Token t, ASTNode node)
            {
                this.oper = t;
                this.expr = node;
            }

            /// <summary>
            /// Get method to return the Token rapresenting this Unary operator 
            /// </summary>
            /// <returns>Return a Token representing this operator </returns>
            public Token getOperator()
            {
                return this.oper;
            }

            /// <summary>
            /// Return the ASTNode representing the operand
            /// </summary>
            /// <returns>Return the ASTNode representing the operand/expression</returns>
            public ASTNode getExpression()
            {
                return this.expr;
            }
        }

        /// <summary>
        /// Node of the abstract syntax tree to model a Assignment operation
        /// An Assigment is made up of :
        ///  - a left operand which is going to be the name of the Variable
        ///  - the assignment Token ':='
        ///  - the right operand which is an expression to evaluate 
        /// </summary>
        public class Assign : ASTNode
        {
            // left is used to store the VARIABLE node
            private Variable leftOperand;
            // t is used to store the ASSIGN Token
            private Token oper;
            // right is used to store node returned by expr()
            private ASTNode rightOperand;

            /// <summary>
            /// Constructor to create an Assign object
            /// </summary>
            /// <param name="left">A Varaiable ASTNode representing the variable</param>
            /// <param name="t">The Token representing the Assignment</param>
            /// <param name="right">An ASTNode representing the expression to evaluate</param>
            public Assign(Variable left, Token t, ASTNode right)
            {
                this.leftOperand = left;
                this.oper = t;
                this.rightOperand = right;
            }

            /// <summary>
            /// Get method to return the left operand
            /// </summary>
            /// <returns>Return an ASTNode Variable representing the left operand</returns>
            public Variable getLeftOperand()
            {
                return this.leftOperand;
            }

            /// <summary>
            /// Get method to return the operator 
            /// </summary>
            /// <returns>Return a Token object representing the Assignment operator</returns>
            public Token getOperator()
            {
                return this.oper;
            }

            /// <summary>
            /// Get method to return the right operand
            /// </summary>
            /// <returns>Return an ASTNode representing the right operand</returns>
            public ASTNode getRightOperand()
            {
                return this.rightOperand;
            }
        }

        /// <summary>
        /// Node of the abstract syntax tree  to model a Variable 
        /// </summary>
        public class Variable : ASTNode
        {
            // The ID Token 
            private Token token;
            // The name of the variable
            private string value;

            /// <summary>
            /// Constructor to create a Varaible object
            /// </summary>
            /// <param name="t">The ID token for this variable</param>
            public Variable(Token t)
            {
                this.token = t;
                this.value = t.getStringValue();
            }

            /// <summary>
            /// Get method to return the Token ID of this variable node
            /// </summary>
            /// <returns>Return the Token ID</returns>
            public Token getTokenID()
            {
                return this.token;
            }

            /// <summary>
            /// Return the name/value of this variable 
            /// </summary>
            /// <returns>Return the String representation of this variable ID</returns>
            public String getValue()
            {
                return this.value;
            }
        }

        /// <summary>
        /// Node of the AST to represent a sine function
        /// </summary>
        public class Sine : ASTNode
        {
            // The sine argument
            private ASTNode sineExpr;

            /// <summary>
            /// Constructor to create a Sine node
            /// </summary>
            /// <param name="sineArg">The argument of the sine function</param>
            public Sine(ASTNode sineArg)
            {
                this.sineExpr = sineArg;
            }

            /// <summary>
            /// Method to get the argument of the sine
            /// </summary>
            /// <returns>The expression representing the argument of the sine</returns>
            public ASTNode getSineArg()
            {
                return this.sineExpr;
            }
        }

        /// <summary>
        /// Node of the AST to represent a cosine function
        /// </summary>
        public class Cosi : ASTNode
        {
            // The cosine argument
            private ASTNode cosiExpr;

            /// <summary>
            /// Constructor to create a cosine node
            /// </summary>
            /// <param name="cosiExpr">The argument of the cosine function</param>
            public Cosi(ASTNode cosiExpr)
            {
                this.cosiExpr = cosiExpr;
            }

            /// <summary>
            /// Method to get the argument of the cosine
            /// </summary>
            /// <returns>The expression representing the argument of the cosine</returns>
            public ASTNode getCosiExpr()
            {
                return this.cosiExpr;
            }
        }

        /// <summary>
        /// Node of the AST to represent a tangent function
        /// </summary>
        public class Tan : ASTNode
        {
            // The tangent argument
            private ASTNode tanExpr;

            /// <summary>
            /// Constructor to create a tangent node
            /// </summary>
            /// <param name="tanExpr"></param>
            public Tan(ASTNode tanExpr)
            {
                this.tanExpr = tanExpr;
            }

            /// <summary>
            /// Method to get the argument of the tangent
            /// </summary>
            /// <returns>The expression representing the argument of the tangent</returns>
            public ASTNode getTanExpr()
            {
                return this.tanExpr;
            }
        }

        /// <summary>
        /// Node of the AST to represent a nthroot function 
        /// </summary>
        public class Nthroot : ASTNode
        {
            // The number and the nroot of the function
            private ASTNode number;
            private ASTNode nroot;

            /// <summary>
            /// Constructor to create a nthroot node
            /// </summary>
            /// <param name="number">The number under the root</param>
            /// <param name="nroot">The degree of the root</param>
            public Nthroot(ASTNode number, ASTNode nroot)
            {
                this.number = number;
                this.nroot = nroot;
            }

            /// <summary>
            /// Get method to return the number under the root
            /// </summary>
            /// <returns>The number under the root</returns>
            public ASTNode getNumber()
            {
                return this.number;
            }

            /// <summary>
            /// Get method to return the degree of the root
            /// </summary>
            /// <returns>The degree of the root</returns>
            public ASTNode getNroot()
            {
                return this.nroot;
            }
        }

        /// <summary>
        /// Node of the AST to represent a factorial function
        /// </summary>
        public class Factorial : ASTNode
        {
            // The argument of the factorial
            private ASTNode number;

            /// <summary>
            /// Constructor to create a factorial node
            /// </summary>
            /// <param name="number">The argument of the factorial function</param>
            public Factorial(ASTNode number)
            {
                this.number = number;
            }

            /// <summary>
            /// Method to return the argument of the factorial 
            /// </summary>
            /// <returns>The argument of the factorial</returns>
            public ASTNode getArgument()
            {
                return this.number;
            }
        }

        /// <summary>
        /// Node of the AST to represent a polynomial
        /// </summary>
        public class Polynomial : ASTNode
        {
            // The expression and the id of the polynomial
            private string expr;
            private Variable idNode;

            /// <summary>
            /// Constructor to create a polynomial node
            /// </summary>
            /// <param name="id">The ID of the polynomial</param>
            /// <param name="expr">The expression of the polynomial</param>
            public Polynomial(Variable id, string expr)
            {
                this.idNode = id;
                this.expr = expr;
            }

            /// <summary>
            /// Get method to return the ID of the polynomial
            /// </summary>
            /// <returns>TThe ID of the polynomial</returns>
            public string getPolyName()
            {
                return this.idNode.getTokenID().getStringValue();
            }

            /// <summary>
            /// Get method to return the expression of the polynomial
            /// </summary>
            /// <returns></returns>
            public string getPolyExpr()
            {
                return this.expr;
            }
        }

        /// <summary>
        /// Node of the AST to represent a plot command
        /// </summary>
        public class Plot : ASTNode
        {
            // The polynomial and the starting and ending point of the plot
            private string poly;
            private int startPoint;
            private int endPoint;

            /// <summary>
            /// Constructor to create a plot node
            /// </summary>
            /// <param name="p">The string representing the polynomial variable ID</param>
            /// <param name="start">The starting point of the plot</param>
            /// <param name="end">The ending point of the plot</param>
            public Plot(string p, int start, int end)
            {
                this.poly = p;
                this.startPoint = start;
                this.endPoint = end;
            }

            /// <summary>
            /// Get method to return the polynomial id 
            /// </summary>
            /// <returns>The ID of the polynomial</returns>
            public string getPolyVar()
            {
                return this.poly;
            }

            /// <summary>
            /// Get method to return the starting point of the plot
            /// </summary>
            /// <returns>The starting point of the plot</returns>
            public int getStartPoing()
            {
                return this.startPoint;
            }

            /// <summary>
            /// Get method to return the ending point of the plot
            /// </summary>
            /// <returns>The ending point of the plot</returns>
            public int getEndPoint()
            {
                return this.endPoint;
            }
        }

        /// <summary>
        /// Node of the AST to represent a minmax command
        /// </summary>
        public class MinMax : ASTNode
        {
            // The polynomial ID from which we need to find the min or max
            private string poly;

            /// <summary>
            /// Constructor to create a MinMax node
            /// </summary>
            /// <param name="polyID">The ID of the polynomial</param>
            public MinMax(string polyID)
            {
                this.poly = polyID;
            }

            /// <summary>
            /// Get method to return the ID of the polynomial
            /// </summary>
            /// <returns>The ID of the polynomial</returns>
            public string getPolyVar()
            {
                return this.poly;
            }
        }
    }
}
