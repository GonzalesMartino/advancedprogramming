﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MathExpressionEngine
{
    /// <summary>
    /// Class to plot a graph of a function/polynomial and display it in a form 
    /// </summary>
    public class PlotGraph : Form
    {
        // I copied the main logic for plotting a graph from a stack overflow comments
        // and adapted it to what I needed to do 
        // Unfortunalty I did not take note of the link from where I took it 
        // and now I cannot find it anymore

        // Define the containers and the Chart 
        private System.ComponentModel.IContainer components = null;
        System.Windows.Forms.DataVisualization.Charting.Chart chart;

        // Initialise the array where we are going to store the data points for the polynomials 
        // and a possible first derivative
        double[,] ployDataPoints;
        double[,] firstDerDataPoints;

        /// <summary>
        /// Constructor to display a chart in which only the polynomials/function is shown
        /// </summary>
        /// <param name="dataPoints">The data points of the polynomials/function to be displayed</param>
        public PlotGraph(double[,] dataPoints)
        {
            this.ployDataPoints = dataPoints;
            this.firstDerDataPoints = null;
            InitializeComponent();
        }

        /// <summary>
        /// Constructor to display a chart representing a polynomial and its first derivative
        /// </summary>
        /// <param name="polyDataPoint">The data points of the polynomials</param>
        /// <param name="firstDerDataPoints">The data points of the first derivatives of the polynomials</param>
        public PlotGraph(double[,] polyDataPoint, double [,] firstDerDataPoints)
        {
            this.ployDataPoints = polyDataPoint;
            this.firstDerDataPoints = firstDerDataPoints;
            InitializeComponent();
        }


        /// <summary>
        /// Methods to load elements in the chart to display
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            // empty the chart
            chart.Series.Clear();
            // Initialise series for the polynomial and add it to the chart 
            var polySerie = new System.Windows.Forms.DataVisualization.Charting.Series
            {
                Name = "polySerie",
                Color = System.Drawing.Color.Green,
                IsVisibleInLegend = false,
                IsXValueIndexed = true,
                ChartType = SeriesChartType.Line
            };
            this.chart.Series.Add(polySerie);

            // Create a new serie for the first derivative and initialise it only if 
            // there are point associated with it 
            System.Windows.Forms.DataVisualization.Charting.Series firstDerSerie = null;
            if (this.firstDerDataPoints != null)
            {
                firstDerSerie = new System.Windows.Forms.DataVisualization.Charting.Series
                {
                    Name = "firstDerSerie",
                    Color = System.Drawing.Color.Red,
                    IsVisibleInLegend = false,
                    IsXValueIndexed = true,
                    ChartType = SeriesChartType.Line
                };
                this.chart.Series.Add(firstDerSerie);
            }
            
            // Iterate throught the data point of the poly/function, and of the first 
            // derivative if it initialised, and add the points to the corrispective series
            int length = this.ployDataPoints.GetLength(1);
            length = length - 1;
            for (int i = 0; i < length; i++)
            {
                polySerie.Points.AddXY(ployDataPoints[0, i], ployDataPoints[1, i]);
                if (firstDerSerie != null)
                {
                    firstDerSerie.Points.AddXY(firstDerDataPoints[0, i], firstDerDataPoints[1, i]);
                }
            }

            chart.Invalidate();
        }

        /// <summary>
        /// Method to dispose of the components of the chart
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Initialise the component of the form
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            this.SuspendLayout();
            // 
            // chart
            // 
            chartArea1.Name = "ChartArea";
            this.chart.ChartAreas.Add(chartArea1);
            this.chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend";
            this.chart.Legends.Add(legend1);
            this.chart.Location = new System.Drawing.Point(0, 0);
            this.chart.Name = "chart";
            this.chart.Size = new System.Drawing.Size(416, 355);
            this.chart.TabIndex = 0;
            this.chart.Text = "chart";
            this.chart.Click += new System.EventHandler(this.chart_Click);
            // 
            // PlotGraph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 355);
            this.Controls.Add(this.chart);
            this.Name = "PlotGraph";
            this.Text = "Plot";
            this.Load += new System.EventHandler(this.Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            this.ResumeLayout(false);

        }

        private void chart_Click(object sender, EventArgs e)
        {

        }
    }
}
