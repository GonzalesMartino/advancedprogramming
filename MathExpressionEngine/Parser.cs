﻿using System;

namespace MathExpressionEngine
{
    /// <summary>
    /// The parser will get the token create from the parser and will 
    /// build a parse tree accordingly with the given tokens
    /// </summary>
    public class Parser
    {
        // Lexer in which are stored all the token 
        public Lexer lexer;
        // Current token to evaluate
        public Token currentToken;


        /// <summary>
        /// Constructor to create a Parser object
        /// </summary>
        /// <param name="l">The Lexer associated with this Parser</param>
        public Parser(Lexer l)
        {
            this.lexer = l;
            this.currentToken = this.lexer.getNextToken();
        }

        /// <summary>
        /// This method it is used to check that the token that is analyzed is 
        /// what we should expect form the grammar. 
        /// </summary>
        /// <param name="givenToken">The token that we expect to parse</param>
        private void consume(TokenTypes givenToken)
        {
            // Compare if the current token is the same as the givenToken
            // and if is then consume it and pass to the next token 
            // Otherwise raise an expection 
            if (this.currentToken.getType() == givenToken)
            {
                if (this.currentToken.getType() != TokenTypes.POLY)
                {
                    // Want to get next token only if is not a polynomial
                    this.currentToken = this.lexer.getNextToken();
                }
            }
            else
            {
                throw new utilities.exceptions.ExpectedTokenMismatchException(givenToken, this.currentToken.getType());
            }
        }

        /// <summary>
        /// Method used to consume all the token in the lexer by simply returning the EOF
        /// </summary>
        private void consume()
        {
            this.currentToken = new Token(TokenTypes.EOF, null);
        }

        /// <summary>
        /// Method that will create a Variable AST node
        /// </summary>
        /// <returns>A Variable AST node</returns>
        private AST.Variable variable()
        {
            AST.Variable variable = new AST.Variable(this.currentToken);
            this.consume(TokenTypes.ID);
            return variable;
        }

        /// <summary>
        /// Method used while parsing a plot node to identify the 
        /// starting and end point of the plot 
        /// </summary>
        /// <returns></returns>
        private int scanPlotBoundry()
        {
            if (this.currentToken.getType() == TokenTypes.MINUS)
            {
                this.consume(TokenTypes.MINUS);
                // Get the number
                AST.Number temp = new AST.Number(this.currentToken);
                // Return the negative number 
                return (int) -temp.getNumber().getIntValue();
            }
            else
            {
                AST.Number temp = new AST.Number(this.currentToken);
                return (int) temp.getNumber().getIntValue();
            }
        }

        /// <summary>
        /// Method to resolve a factor. A factor is specified as 
        /// Factor : PLOT
        ///         | MINMAX
        ///         | PLUS factor
        ///         | MINUS factor
        ///         | NUMBER
        ///         | FACTORIAL
        ///         | SIN
        ///         | COS
        ///         | TAN
        ///         | NTHROOT
        ///         | LPAREN expr RPAREN
        ///         | Variable
        /// </summary>
        /// <returns>Return an ASTNode representing this factor</returns>
        private AST.ASTNode factor()
        {
            Token t = this.currentToken; 

            // First of all we will check if it is a plot
            if (t.getType() == TokenTypes.PLOT)
            {
                this.consume(TokenTypes.PLOT);
                this.consume(TokenTypes.LPAREN);
                // Get polynomial variable name
                string polyVarName = this.currentToken.getStringValue();
                // Consume the ID token
                this.consume(TokenTypes.ID);
                // need to check if next token is a ',' in the case the start and end point are specified
                // by default we are going to plot from -10 to 10
                int start=-10, end=10;
                if (this.currentToken.getType() == TokenTypes.SEPARATOR)
                {
                    this.consume(TokenTypes.SEPARATOR);
                    // Get the start point of the plot
                    start = this.scanPlotBoundry();
                    // Consume the start point and next separator
                    this.consume(TokenTypes.NUMBER);
                    this.consume(TokenTypes.SEPARATOR);
                    // Get the end point of the plot 
                    end = this.scanPlotBoundry();
                    // Consume the end point
                    this.consume(TokenTypes.NUMBER);
                }
                this.consume(TokenTypes.RPAREN);
                // Create plot node and return it
                AST.ASTNode plotNode = new AST.Plot(polyVarName, start, end);
                return plotNode;
            }
            else if (t.getType() == TokenTypes.MINMAX) // In case it is a minmax token 
            {
                this.consume(TokenTypes.MINMAX);
                this.consume(TokenTypes.LPAREN);
                // Get polynomial variable name
                string polyVarName = this.currentToken.getStringValue();
                this.consume(TokenTypes.ID);
                this.consume(TokenTypes.RPAREN);
                // Create minmax node and return it
                AST.ASTNode minMaxNode = new AST.MinMax(polyVarName);
                return minMaxNode;
            }
            else if (t.getType() == TokenTypes.PLUS) // If it is a plus sign
            {
                // Consume the PLUS sign and return a UnaryOp Node with next factor
                this.consume(TokenTypes.PLUS);
                AST.ASTNode node = new AST.UnaryOp(t, this.factor());
                return node;
            }
            else if (t.getType() == TokenTypes.MINUS) // If it is a minus sign
            {
                // Consume the MINUS sing and return an UnaryOp node with next factor
                this.consume(TokenTypes.MINUS);
                AST.ASTNode node = new AST.UnaryOp(t, this.factor());
                return node;
            }
            else if (t.getType() == TokenTypes.NUMBER) // If it is a number
            {
                // Consume the NUMBER Token and return a Number node
                this.consume(TokenTypes.NUMBER);
                AST.Number number = new AST.Number(t);
                // If next token is a FACTORIAL Token return a FACTORIAL node
                if(this.currentToken.getType() == TokenTypes.FACTORIAL) 
                {
                    this.consume(TokenTypes.FACTORIAL);
                    return new AST.Factorial(number);
                }
                return number;
            }
            else if (t.getType() == TokenTypes.SIN) // If it is a sin
            {
                // Consume a SIN token and return a Sine node
                this.consume(TokenTypes.SIN);
                AST.ASTNode node = this.factor();
                return new AST.Sine(node);
            }
            else if (t.getType() == TokenTypes.COS) // If it is a cos
            {
                // Consume a COS token and return a cosi node
                this.consume(TokenTypes.COS);
                AST.ASTNode node = this.factor();
                return new AST.Cosi(node);
            }
            else if (t.getType() == TokenTypes.TAN) // If it is a tan 
            {
                // Consume a TAN token and return a tan node
                this.consume(TokenTypes.TAN);
                AST.ASTNode node = this.factor();
                return new AST.Tan(node);
            }
            else if (t.getType() == TokenTypes.NTHROOT) // If it is a nthroot 
            {
                // Consume all the nthroot token in correct order and return the 
                // nthroot node
                this.consume(TokenTypes.NTHROOT);
                this.consume(TokenTypes.LPAREN);
                AST.ASTNode number = this.factor();
                this.consume(TokenTypes.SEPARATOR);
                AST.ASTNode root = this.factor();
                this.consume(TokenTypes.RPAREN);
                return new AST.Nthroot(number, root);
            }
            else if (t.getType() == TokenTypes.LPAREN) // If it is a (
            {
                // Consume the LPAREN token, create a node corresponding to 
                // the expression inside the brackets, consume the RPAREN
                this.consume(TokenTypes.LPAREN);
                AST.ASTNode node = this.expr();
                this.consume(TokenTypes.RPAREN);
                return node;
            }
            else
            {
                // Create a variable ASTNode and return it 
                AST.ASTNode node = this.variable();
                return node;
            }
        }

        /// <summary>
        /// Method to resolve a term. A term is specified with the following grammar
        /// term :  POLY 
        ///         | EXP factor
        ///         | FACTORIAL
        ///         | factor ((MUL|DIV|MOD) factor)*
        /// </summary>
        /// <returns>Return an ASTNode representing this term </returns>
        private AST.ASTNode term()
        {
            // Evaluate the factor and return it as an ASTNode
            AST.ASTNode node = this.factor();

            // Test in case a separator is used in the wrong context 
            if (this.currentToken.getType() == TokenTypes.SEPARATOR)
            {
                throw new utilities.exceptions.WrongUseOfSeparatorException();
            } 
            else if (this.currentToken.getType() == TokenTypes.POLY) // If poly
            {
                this.consume(TokenTypes.POLY);
                // Need to get the rest of the input as string at this moment so to store it in the AST node
                string expr = this.lexer.getRestOfInput();
                AST.ASTNode polyNode = new AST.Polynomial((AST.Variable)node, expr);
                // Consume all the input
                this.consume();
                return polyNode;
            }
            else if (this.currentToken.getType() == TokenTypes.EXP) // Exponential
            {
                // The the exponent of the exponential 
                this.consume(TokenTypes.EXP);
                node = new AST.Exp(node, this.factor());
            }
            else if (this.currentToken.getType() == TokenTypes.FACTORIAL) // Factorial
            {
                // Create a factorial node with the previouly created node
                this.consume(TokenTypes.FACTORIAL);
                node = new AST.Factorial(node);
            }

            // Keep looping if there are multiplication,division or modulo operation 
            while (this.currentToken.getType() == TokenTypes.MUL || 
                   this.currentToken.getType() == TokenTypes.DIV || 
                   this.currentToken.getType() == TokenTypes.MOD )
            {
                // Get the current token and consume it accordingly to the operation 
                Token t = this.currentToken;
                if (t.getType() == TokenTypes.MUL)
                {
                    this.consume(TokenTypes.MUL);
                }
                else if (t.getType() == TokenTypes.DIV)
                {
                    this.consume(TokenTypes.DIV);
                }
                else if (t.getType() == TokenTypes.MOD)
                {
                    this.consume(TokenTypes.MOD);
                }

                // Create a BinaryOp Node giving him the previously calculated factor and the next factor 
                AST.ASTNode newNode = this.factor();
                // Check for exponential and factorial inside concatenation of binary operation
                if (this.currentToken.getType() == TokenTypes.EXP) // Exponential
                {
                    this.consume(TokenTypes.EXP);
                    node = new AST.BinaryOp(node, t, new AST.Exp(newNode, this.factor()));
                }
                else if (this.currentToken.getType() == TokenTypes.FACTORIAL) // Factorial
                {
                    this.consume(TokenTypes.FACTORIAL);
                    node = new AST.BinaryOp(node, t, new AST.Factorial(newNode));
                }
                else
                {
                    node = new AST.BinaryOp(node, t, newNode);
                }
            }
            return node;
        }

        /// <summary>
        /// Method to resolve an expression. An expression is specified with the following grammar
        /// expr : ASSIGN
        ///         | term ((PLUS|MINUS) term)*
        /// </summary>
        /// <returns>Return an ASTNode representing this expression </returns>
        private AST.ASTNode expr()
        {

            // Return the first term of this expression 
            AST.ASTNode node = this.term();

            // Check if the current token is an ASSIGN token and resolve it
            if (this.currentToken.getType() == TokenTypes.ASSIGN)
            {

                AST.ASTNode variableNode = node;
                Token t = this.currentToken;
                this.consume(TokenTypes.ASSIGN);
                AST.ASTNode exprNode = this.expr();
                return new AST.Assign((AST.Variable)variableNode, t, exprNode);
            }

            // Keep looping if there are PLUS or MINUS operation 
            while (this.currentToken.getType() == TokenTypes.PLUS || this.currentToken.getType() == TokenTypes.MINUS)
            {
                // Consume current token accordingly
                Token t = this.currentToken;
                if (t.getType() == TokenTypes.PLUS)
                {
                    this.consume(TokenTypes.PLUS);
                }
                else if (t.getType() == TokenTypes.MINUS)
                {
                    this.consume(TokenTypes.MINUS);
                }
                // Create a BinaryOp node 
                node = new AST.BinaryOp(node, t, this.term());
            }
            
            // Return a ASTNode structrue tree representing this expression 
            return node;
        }

        /// <summary>
        /// Method to parse an expression and build a tree structure made of ASTNode
        /// </summary>
        /// <returns>Return a tree structor representing this expression </returns>
        public AST.ASTNode parse()
        {
            AST.ASTNode result = this.expr();
            if (lexer.getOpenParStack().Count != 0)
            {
                throw new utilities.exceptions.UnmatchParenthesesException();
            }
            return result;
        }
    }
}
