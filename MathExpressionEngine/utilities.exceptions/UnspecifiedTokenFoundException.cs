﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle if an unspecified token is found
    /// </summary>
    public class UnspecifiedTokenFoundException : Exception
    { 
        public UnspecifiedTokenFoundException(char token)
            : base("Unspecified Token Found : " + token)
        {}
    }
}
