﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle if the argument given to the plot method 
    /// is not a function
    /// </summary>
    public class InvalidArgumentForPlotException : Exception
    {
        public InvalidArgumentForPlotException()
            : base("The argument given to the plot function is invalid. Remeber it must be a function")
        { }
    }
}
