﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle if an expression results in a division by 0
    /// </summary>
    public class  DivideByZeroException : Exception
    {
        public DivideByZeroException(string numerator, string denominator)
            : base("Impossible to divide by zero : " + numerator + "/" + denominator)
        { }
    }
}
