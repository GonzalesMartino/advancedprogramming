﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle an invalid argument for a factorial
    /// </summary>
    public class InvalidArgumentFactorialException : Exception
    {
        public InvalidArgumentFactorialException(string argument)
            : base("Invalid argument for factorial : " + argument)
        { }
    }
}
