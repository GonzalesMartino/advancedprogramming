﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle if there is a mismatch between the expected and found token
    /// </summary>
    public class ExpectedTokenMismatchException : Exception
    {
        public ExpectedTokenMismatchException(TokenTypes expected, TokenTypes found)
            : base("Found token " + found + " but was expecting token " + expected)
        { }
    }
}
