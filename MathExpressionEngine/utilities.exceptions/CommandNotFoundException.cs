﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle if the user input a command which is not specified
    /// </summary>
    public class CommandNotFoundException : Exception
    {
        public CommandNotFoundException(string command)
            : base("The given command " + command + " is not specified")
        { }
    }
}
