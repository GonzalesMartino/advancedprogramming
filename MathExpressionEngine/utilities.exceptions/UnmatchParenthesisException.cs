﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle in case there is a mismatch of open/closed parentheses
    /// </summary>
    public class UnmatchParenthesesException : Exception
    {
        public UnmatchParenthesesException()
            : base("Unmatch parentheses found")
        { }
    }
}
