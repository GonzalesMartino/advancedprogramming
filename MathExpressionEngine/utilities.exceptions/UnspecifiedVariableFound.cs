﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle if a variable that was not specified is tried to be used
    /// </summary>
    public class UnspecifiedVariableFound : Exception
    {
        public UnspecifiedVariableFound(string variable)
            : base("Unspecified variable found : " + variable)
        { }
    }
}
