﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle in case is not possible to compute 
    /// the root of the given number
    /// </summary>
    public class InvalidRootArgumentException : Exception
    {
        public InvalidRootArgumentException(double number, double root) 
            : base("Invalid input for the specified root. nthroot("+number+","+root+")")
            { }
    }


}
