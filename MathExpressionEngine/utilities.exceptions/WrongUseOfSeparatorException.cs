﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle in case the separator ',' is used in a wrong manner
    /// </summary>
    public class WrongUseOfSeparatorException : Exception
    {
        public WrongUseOfSeparatorException() 
            : base("A separator ',' is used wrongly in the expression")
        { }
    }
}
