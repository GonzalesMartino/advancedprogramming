﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle in case the polynomial expression for minmax function is not in the correct format
    /// </summary>
    public class WrongPolyExpressionFormatForMinMax : Exception
    {
        public WrongPolyExpressionFormatForMinMax(string expr)
            : base ("The polynomial expression, " + expr + ", is not in the allowed form. The polynomial format need to be a*x^n+ b*x^(n-1)+...+c*x^1+d")
        { }
    }
}
