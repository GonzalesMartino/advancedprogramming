﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    public class VariableAlreadyDefinedExceptions : Exception
    {
        public VariableAlreadyDefinedExceptions()
            : base("Variable was already defined in the system and cannot be reused as a variable for a polynomial/function")
        { }
    }
}
