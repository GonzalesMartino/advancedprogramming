﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle in case a given polynomial was not previously specified
    /// </summary>
    public class NoPolynomialFoundException : Exception
    {
        public NoPolynomialFoundException(string poly)
            : base("The given polynomial ID " + poly + " was not found")
        { }
    }
}
