﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception in case no input is entered 
    /// </summary>
    public class NoInputEnteredException : Exception
    {
        public NoInputEnteredException()
            : base("No Input has been entered.")
        { }
    }
}
