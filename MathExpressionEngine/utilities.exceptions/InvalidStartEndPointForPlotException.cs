﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle in case the start and end point of plot method are invalid
    /// </summary>
    public class InvalidStartEndPointForPlotException : Exception
    {
        public InvalidStartEndPointForPlotException(int start, int end)
            : base("The starting point, " + start + ", must be less then the end point, " + end)
        { }
    }
}
