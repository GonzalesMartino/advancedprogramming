﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle in case a polynomial expression does not have 
    /// a variable in it 
    /// </summary>
    public class NoPolynomialVariableFoundException : Exception
    {
        public NoPolynomialVariableFoundException(string polyExpr)
            : base("No variable for the polynomian, " + polyExpr + ", was found. Remember variable need to be uppercase")
        { }
    }
}
