﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine.utilities.exceptions
{
    /// <summary>
    /// Custom exception to handle in case an expression result in taking the module of 0
    /// </summary>
    public class ModByZeroException : Exception
    {
        public ModByZeroException()
            : base("Impossible to take the module of 0 from a number")
        { }
    }
}
