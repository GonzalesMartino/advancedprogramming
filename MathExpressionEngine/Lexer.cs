﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MathExpressionEngine
{
    /// <summary>
    /// The Lexer class, or tokenizer, it is responsible
    /// for scanning the input from the user and grouping characters
    /// that belongs together in meaningful tokens.
    /// </summary>
    public class Lexer
    {
        // Text is the input from the user
        private string text;
        // The index on which we are in text
        private int pos;
        // The current char that it is analysing 
        private char currentChar;
        // Add variable to keep track if a parenthesis was open closed correctly
        private Stack<bool> openPar;

        /// <summary>
        /// Constructor for a Lexer object
        /// </summary>
        /// <param name="input">The input from the user</param>
        public Lexer(string input)
        {

            // Remove all spaces form the polyExpression
            input = Regex.Replace(input, @"\s+", "");

            // In case input length is 0 then no input was entered
            if (input.Length == 0)
            {
                throw new utilities.exceptions.NoInputEnteredException();
            }

            // Initialise variable 
            this.text = input;
            this.pos = 0;
            this.currentChar = this.text[this.pos];
            this.openPar = new Stack<bool>();
        }

        /// <summary>
        /// Methods to retunr the stack of boolean which is going to 
        /// be used to test if there was a mismatch of open/closed parentheses
        /// </summary>
        /// <returns>The stack of bool variable associate for each open parenthesis</returns>
        public Stack<bool> getOpenParStack()
        {
            return this.openPar;
        }

        /// <summary>
        /// Private method to advance the position of this.pos
        /// In case the position exceed the length of text 
        /// the current char is set to '\0' ( equivalent to set char to null )
        /// Otherwise the current char is set to the new char indexed by pos
        /// </summary>
        private void advance()
        {
            this.pos++;

            if (this.pos > this.text.Length - 1) // we reached the EOF
            {
                this.currentChar = '\0'; // Equivalent to set char to null
            }
            else
            {
                this.currentChar = this.text[this.pos];
            }
        }

        /// <summary>
        /// Private method to scan a number (Integer or Double) from the input text 
        /// and return its string representation 
        /// </summary>
        /// <returns>Return the string representation of the number scanned</returns>
        private string scanNumber()
        {
            string temp = "";

            // Keep looping if the current char is a digit or a '.', meaning that has a decimal part
            // or until the end of the input is reached 
            while (this.currentChar != '\0' & (Char.IsDigit(this.currentChar) || this.currentChar == '.'))
            {
                temp += this.currentChar;
                this.advance();
            }

            return temp;
        }

        /// <summary>
        /// Method to return an ID Token.
        /// An ID Token is generated in case the user is trying to define a variable 
        /// or in case it scanned a keyword of the interpreter ( ex: sin,cos,root ...)
        /// </summary>
        /// <returns>It return a Token relative to the string scanned</returns>
        private Token id()
        {
            string result = "";

            // loop to get the string 
            while (this.currentChar != '\0' && char.IsLetterOrDigit(this.currentChar))
            {
                result += this.currentChar;
                this.advance();
            }

            // In case it start with an upper char means that it is relative to a 
            // variable specified by the user
            if (char.IsUpper(result[0]))
            {
                // Create a token ID with the specified name for that variable 
                return new Token(TokenTypes.ID, result);
            }
            else
            {
                if (result.CompareTo("sin") == 0) 
                {
                    return new Token(TokenTypes.SIN, "sin");
                }
                else if (result.CompareTo("cos") == 0)
                {
                    return new Token(TokenTypes.COS, "cos");
                }
                else if (result.CompareTo("tan") == 0)
                {
                    return new Token(TokenTypes.TAN, "tan");
                }
                else if (result.CompareTo("nthroot") == 0)
                {
                    return new Token(TokenTypes.NTHROOT, "nthroot");
                }
                else if(result.CompareTo("plot")==0)
                {
                    return new Token(TokenTypes.PLOT, null);
                }
                else if (result.CompareTo("minmax")==0)
                {
                    return new Token(TokenTypes.MINMAX, null);
                }

                // In case user entered a not specified command
                throw new utilities.exceptions.CommandNotFoundException(result);
            }
        }

        // Used to scan rest of input and return it as a string
        // Need it for POLY
        /// <summary>
        /// Method used to get all the rest of the input. 
        /// Used for solving POLY token 
        /// </summary>
        /// <returns>The rest of the input</returns>
        public string getRestOfInput()
        {
            string temp = this.text;
            string rest = temp.Substring(this.pos);
            return rest;
        }


        /// <summary>
        /// Method that scan the input and create Token accordingly 
        /// </summary>
        /// <returns>The next Token encountered in the input</returns>
        public Token getNextToken()
        {
            // Create a temp copy of the input
            string tempText = this.text;

            // Test if the index position passed the end of the input
            if (this.pos > tempText.Length - 1)
            {
                return new Token(TokenTypes.EOF, null);
            }

            // Get the char at the position of pos
            // So that we can decide what to do with it 
            this.currentChar = tempText[this.pos];

            // In case the current char is a letter it means it is associated with 
            // a ID Token ( variable or keyword )
            if (Char.IsLetter(this.currentChar))
            {
                return this.id();
            }

            // If the char is a digit then create a NUMBER Token  with its value 
            if (Char.IsDigit(currentChar))
            {
                return new Token(TokenTypes.NUMBER, this.scanNumber());
            }

            // Test if char a '+' and create is PLUS token
            if (currentChar == '+') 
            {
                this.pos++;
                return new Token(TokenTypes.PLUS, "+");
            }
            else if (currentChar == '-') // Test if char is '-' and create a MINUS token
            {
                this.pos++;
                return new Token(TokenTypes.MINUS, "-");
            }
            else if (currentChar == '*') // Test if char is '*' and create a MUL token
            {
                this.pos++;
                return new Token(TokenTypes.MUL, "*");
            }
            else if (currentChar == '/') // Test if char is '/' and create a DIV token
            {
                this.pos++;
                return new Token(TokenTypes.DIV, "/");
            }
            else if (currentChar == '(') // Test if char is '(' and create a LPAREN token
            {
                this.openPar.Push(true);
                this.pos++;
                return new Token(TokenTypes.LPAREN, "(");
            }
            else if (currentChar == ')') // Test if char is ')' and create a RPAREN token
            {
                // if openPar count is 0 means that no parenthesis was opened an so there is
                // an unmatched closing parenthesis 
                if (openPar.Count == 0)
                {
                    throw new utilities.exceptions.UnmatchParenthesesException();
                }
                this.pos++;
                // Pop out a true bolean form openPar stack as we are closing it
                openPar.Pop();
                return new Token(TokenTypes.RPAREN, ")");
            }
            else if (this.currentChar == '^') // Test if char is '^' and create a EXP token
            {
                this.pos++;
                return new Token(TokenTypes.EXP, "^");
            }
            else if (this.currentChar == ',') // Test if char is ',' and create a SEPARATOR token
            {
                this.pos++;
                return new Token(TokenTypes.SEPARATOR, ",");
            }
            else if (this.currentChar == '%') // Test if char is '%' and create a MOD token
            {
                this.pos++;
                return new Token(TokenTypes.MOD, "%");
            }
            else if (this.currentChar == '!') // Test if char is '!' and create a FACTORIAL token
            {
                this.pos++;
                return new Token(TokenTypes.FACTORIAL, "!");
            }
            else if (this.currentChar == ':' )
            {
                this.advance(); // pass ':'
                if (this.currentChar == '=') // Test if it is ':=' and create a ASSIGN token
                {
                    this.advance(); // pass '='
                    return new Token(TokenTypes.ASSIGN, ":=");
                }
                else
                {
                    // Otherwise create a POLY token
                    return new Token(TokenTypes.POLY, ":");
                }
                
            }

            // In case no token was found then there is an unspecified token 
            throw new utilities.exceptions.UnspecifiedTokenFoundException(this.currentChar);
        }
    }
}
