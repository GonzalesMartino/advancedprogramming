﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpressionEngine
{
    /// <summary>
    /// Enumerator in which we define all the possible Token we are going to have for 
    /// our interpreter 
    /// </summary>
    public enum TokenTypes
    {
        EOF,
        NUMBER,
        PLUS,
        MINUS,
        MUL,
        DIV,
        LPAREN,
        RPAREN,
        ID,
        ASSIGN,
        EXP,
        SIN,
        COS,
        TAN,
        NTHROOT,
        SEPARATOR,
        MOD,
        FACTORIAL,
        POLY,
        PLOT,
        MINMAX

    }

    /// <summary>
    /// Class that model a Token instance, which are used to interpret the input form the user 
    /// </summary>
    public class Token
    {
        /* 
         * Possible Token 
         * Token(EOF,null)
         * Token(NUMBER,"stringValueOfNumber")
         * Token(PLUS,"+")
         * Token(MINUS,"-")
         * Token(MUL,"*")
         * Token(DIV,"/")
         * Token(LPAREN,"(")
         * Token(RPAREN,")")
         * Token(ID,"variableName")
         * Token(ASSIGN, ":=")
         * Token(EXP,"^")
         * Token(SIN,"sin")
         * Token(COS,"cos")
         * Token(TAN,"tan")
         * Token(NTHROOT,"nthroot")
         * Token(SEPARATOR,",")
         * Token(MOD,"%")
         * Token(FACTORIAL,"!")
         * Token(POLY,":")
         * Token(PLOT,"poly")
         * Token(MINMAX,"poly")
         */

        // Value associated with this token 
        private string value;
        // The token types
        private TokenTypes type;


        /// <summary>
        /// Constructor to create a Token object
        /// </summary>
        /// <param name="type">The TokenTypes of this object</param>
        /// <param name="value">The string representation/value of this token in the input</param>
        public Token(TokenTypes type, string value)
        {
            this.type = type;
            this.value = value;
        }

        /// <summary>
        /// Method to return the token types of this Token 
        /// </summary>
        /// <returns>This TokenType</returns>
        public TokenTypes getType()
        {
            return this.type;
        }

        /// <summary>
        /// Return the string representation of this Token 
        /// </summary>
        /// <returns>Return the value of this token </returns>
        public String getStringValue()
        {
            return this.value;
        }

        /// <summary>
        /// Return the number representation of this Token
        /// </summary>
        /// <returns>Return the double representation of this Token</returns>
        public double getIntValue()
        {
            return double.Parse(this.value);
        }
    }
}
