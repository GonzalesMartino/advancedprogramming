﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MathExpressionEngine
{


    /// <summary>
    /// Class that will interpret a ASTNode tree structor and evaluate the result of it 
    /// </summary>
    public class Interpreter
    {
        // Static Dictionary in which we are going to store the variable and the polynomials defined by the user
        public static Dictionary<string, string> GLOBAL_VAR = new Dictionary<string, string>();

        // Parser variable that will be used to create the tree structure representing the input form the user
        private Parser parser;

        /// <summary>
        /// Constructor for creating an Interpreter object given a Parser object
        /// </summary>
        /// <param name="p">A Parser object representing the input from the user</param>
        public Interpreter(Parser p)
        {
            this.parser = p;
        }

        /// <summary>
        /// Method that will interpret and evaluate the tree from the parser
        /// </summary>
        /// <returns>The result fo the tree evaluation</returns>
        public dynamic interpret()
        {
            // Create the tree from the parser
            AST.ASTNode tree = parser.parse();

            // Visit the tree and return its evaluation 
            return this.visit(tree);
        }

        /// <summary>
        /// Method to visit and detect what type of node it is so that can be evaluated accordingly 
        /// </summary>
        /// <param name="node">The ASTNode to be evaluated</param>
        /// <returns>Evaluate the ASTNode and return its result</returns>
        private dynamic visit(AST.ASTNode node)
        {
            // Get the name of this node so we can apply sepcific method
            string methodName = "visit_" + node.GetType().Name;
            // Needed to use BindingFlags in order to access private method using reflection 
            MethodInfo method = this.GetType().GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);
            // Create the parameters for the method and invoke the method 
            AST.ASTNode[] param = { node };

            try
            {
                return (dynamic)method.Invoke(this, param);
            }
            catch (TargetInvocationException e)
            {
                // Need to do like this since I throw an exception form a method.Invoke reflection
                // https://stackoverflow.com/questions/4117228/reflection-methodinfo-invoke-catch-exceptions-from-inside-the-method
                // https://stackoverflow.com/questions/57383/how-to-rethrow-innerexception-without-losing-stack-trace-in-c/17091351#17091351
                // however I do not get it why it does not recognise the throw and ask for a return statement as well
                ExceptionDispatchInfo.Capture(e.InnerException).Throw();
                // Dummy return statement to make the code compile anyway 
                return null;
            }
        }

        /// <summary>
        /// Method to visit and evaluate a BinaryOp node
        /// </summary>
        /// <param name="node">The BinaryOp node to be evaluated</param>
        /// <returns>The results of this BinaryOp node</returns>
        private double visit_BinaryOp(AST.BinaryOp node)
        {
            double result = 0;
            //  if statements block to check which binary operation is and evaluate it accordinhly
            if (node.getOperator().getType() == TokenTypes.PLUS)
            {
                result = this.visit(node.getLeftOperand()) + this.visit(node.getRightOperand());
            }
            else if (node.getOperator().getType() == TokenTypes.MINUS)
            {
                result = this.visit(node.getLeftOperand()) - this.visit(node.getRightOperand());
            }
            else if (node.getOperator().getType() == TokenTypes.MUL)
            {
                result = this.visit(node.getLeftOperand()) * this.visit(node.getRightOperand());
            }
            else if (node.getOperator().getType() == TokenTypes.DIV)
            {
                double numerator = this.visit(node.getLeftOperand());
                double denominator = this.visit(node.getRightOperand());
                // Test if division by 0
                if (denominator == 0)
                {
                    throw new utilities.exceptions.DivideByZeroException(numerator.ToString(), denominator.ToString());
                }
                result = numerator / denominator;
            }
            else if (node.getOperator().getType() == TokenTypes.MOD)
            {
                double argument = this.visit(node.getLeftOperand());
                double moduleArg = this.visit(node.getRightOperand());
                // Test if we are taking the module of 0 
                if (moduleArg == 0)
                {
                    throw new utilities.exceptions.ModByZeroException();
                }
                result = argument % moduleArg;
            }
            return result;
        }

        /// <summary>
        /// Method to visit an UnaryOp node
        /// </summary>
        /// <param name="node">The given UnaryOp node</param>
        /// <returns>The result of this UnaryOp node</returns>
        private double visit_UnaryOp(AST.UnaryOp node)
        {
            // Get the operation token and check how we need to 
            // evaluate this unary node
            Token op = node.getOperator();
            double result = 0;
            if (op.getType() == TokenTypes.PLUS)
            {
                result = +this.visit(node.getExpression());
            }
            else if (op.getType() == TokenTypes.MINUS)
            {
                result = - this.visit(node.getExpression());
            }
            return result;
        }

        /// <summary>
        /// Method to visit a Number node
        /// </summary>
        /// <param name="node">The Number node to be visited</param>
        /// <returns>The result of visiting this Number node</returns>
        private double visit_Number(AST.Number node)
        {
            return node.getNumber().getIntValue();
        }

        /// <summary>
        /// Method to visit an Exp node
        /// </summary>
        /// <param name="node">The Exp node to be visited and evaluated</param>
        /// <returns>The result of visiting this Exp node</returns>
        private double visit_Exp(AST.Exp node)
        {
            return Math.Pow(this.visit(node.getBaseExpr()), this.visit(node.getExp()));
        }

        /// <summary>
        /// Method to visit a Sine node
        /// </summary>
        /// <param name="node">The sine node to be visited and evaluated</param>
        /// <returns>The result of visiting this Sine node</returns>
        private double visit_Sine(AST.Sine node)
        {
            // Get the argument of the sine operation
            double sinArg = this.visit(node.getSineArg());
            // Convert it to radiant since Math.Sin calculate the sine using radiants 
            double radiant = (sinArg * (Math.PI)) / 180;
            // Get the result of the sine 
            double result = Math.Sin(radiant);
            // If statement to test if it is close to zero 
            // otherwise would return a double precision 
            if (Math.Abs(result) < 0.00001)
            {
                return 0;
            }
            return result;
        }

        /// <summary>
        /// Method to visit a Cosi node
        /// </summary>
        /// <param name="node">The Cosi node to be visited and evaluated</param>
        /// <returns>The result of visiting this Cosi node</returns>
        private double visit_Cosi(AST.Cosi node)
        {
            // Get the argument of the sine operation
            double cosArg = this.visit(node.getCosiExpr());
            // Convert it to radiant since Math.Sin calculate the sine using radiants 
            double radiant = (cosArg * (Math.PI)) / 180;
            // Get the result of the sine 
            double result = Math.Cos(radiant);
            // If statement to test if it is close to zero 
            // otherwise would return a double precision 
            if (Math.Abs(result) < 0.00001)
            {
                return 0;
            }
            return result;
        }

        /// <summary>
        /// Method to visit a Tan node
        /// </summary>
        /// <param name="node">The Tan node to be visited and evaluated</param>
        /// <returns>The result of visiting this Tan node</returns>
        private double visit_Tan(AST.Tan node)
        {
            // Calculate the tan as sin/cos
            // Get the argument of the tan operation
            double arg = this.visit(node.getTanExpr());
            // Convert it to radiant since Math.Sin calculate the sine using radiants 
            double radiant = (arg * (Math.PI)) / 180;
            // Get the result of the sine as numerator 
            double numerator = Math.Sin(radiant);
            // Get the result of cos as denominator 
            double denominator = Math.Cos(radiant);
            if (Math.Abs(denominator) < 0.00001)
            {
                denominator = 0;
            }
            // Check if result of the cos is 0 
            if (denominator == 0)
            {
                throw new utilities.exceptions.DivideByZeroException("sin("+ arg.ToString()+")","cos("+ arg.ToString()+")");
            }
            double result = numerator / denominator;
            // If statement to test if it is close to zero 
            // otherwise would return a double precision 
            if (Math.Abs(result) < 0.00001)
            {
                return 0;
            }
            return result;
        }

        /// <summary>
        /// Method to visit a Nthroot node
        /// </summary>
        /// <param name="node">The Nthroot node to be visited and evaluated</param>
        /// <returns>The result of visiting this Nthroot</returns>
        private double visit_Nthroot(AST.Nthroot node)
        {
            // Get the argument of the root and the root degree
            double number = this.visit(node.getNumber());
            double root = this.visit(node.getNroot());
            // Test if the nthroot is accettable to be calculated
            if (root%2==0 && number < 0 || root == 0 || root < 0 && number == 0 )
            {
                throw new utilities.exceptions.InvalidRootArgumentException(number, root);
            }
            // Get exponent associated to the root degree
            double exponent = 1.0 / root;
            double result;
            if (root == 3 && number<0)
            {
                // Needed this if statement otherwise would return NaN given the 
                // implementation of Math.Pow
                number = number * -1;
                result = Math.Pow(number, exponent);
                result = result * -1;
            }
            else
            {
                result = Math.Pow(number, exponent);
            }
            return result;
        }

        /// <summary>
        /// Method to visit a Factorial node
        /// </summary>
        /// <param name="node">The Factorial node to be visited and evaluated</param>
        /// <returns>The result of visiting this Factorial</returns>
        private double visit_Factorial(AST.Factorial node)
        {

            // Get the argument of the factorial
            double arg = this.visit(node.getArgument());
            // Test if argument of factorial has decimal part
            if(arg%1!=0)
            {
                throw new utilities.exceptions.InvalidArgumentFactorialException(arg.ToString());
            } 
            else if (arg < 0) // Test if it is less then 0
            {
                throw new utilities.exceptions.InvalidArgumentFactorialException(arg.ToString());
            }
            else if (arg == 0) // Test if it 0
            {
                return 1;
            }
            // Compute the factorial
            int result = (int) arg;
            for (int i = result - 1 ; i >= 1; i--)
            {
                result *= i;
            }
            return result;
        } 

        /// <summary>
        /// Method to visit an Assign node
        /// </summary>
        /// <param name="node">The Assign node to be visited</param>
        /// <returns>The result of this Assign </returns>
        private string visit_Assign(AST.Assign node)
        {
            // Get name of the variable
            string varName = node.getLeftOperand().getTokenID().getStringValue();
            // Get value of the variable
            double varValue = this.visit(node.getRightOperand());
            // Create variable 
            GLOBAL_VAR[varName] = varValue.ToString();
            return varName + ":=" + GLOBAL_VAR[varName];
        }

        /// <summary>
        /// Method to visit a Polynomial node
        /// </summary>
        /// <param name="node">The Polynomial node to be visited</param>
        /// <returns>The result of this Polynomial</returns>
        private string visit_Polynomial(AST.Polynomial node)
        {
            // Get the polynomial name
            string polyName = node.getPolyName();
            // Store the polynomial expression with its name
            GLOBAL_VAR[polyName] = node.getPolyExpr();
            return polyName + ':' + GLOBAL_VAR[polyName];
        }

        /// <summary>
        /// Method to visit a Plot node
        /// </summary>
        /// <param name="node">The Plot node to be visited</param>
        /// <returns>The result of this plot</returns>
        private string visit_Plot(AST.Plot node)
        {
            // Get the poly name and the start and end point of the plot
            string polyName = node.getPolyVar();
            int startPoint = node.getStartPoing();
            int endPoint   = node.getEndPoint();
            // Try to get the expression associated with the poly name
            string polyExpr;
            try
            {
                polyExpr = GLOBAL_VAR[polyName];
            }
            catch (Exception e)
            {
                throw new utilities.exceptions.NoPolynomialFoundException(polyName);
            }
            // Check if the vairable to plot is a polynomial or not
            // In case it can be parsed means that is a number so it is not 
            // a polynomial/function
            double notUsed;
            if (double.TryParse(polyExpr, out notUsed))
            {
                // Throw exception variable is not a polynomial
                throw new utilities.exceptions.InvalidArgumentForPlotException();
            }
            // Find the variable of the polynomial 
            char exprVar = this.getPolyVariable(polyExpr);
            // Test that start is less then end point 
            if (startPoint>=endPoint)
            {
                throw new utilities.exceptions.InvalidStartEndPointForPlotException(startPoint, endPoint);
            }
            // Calculate the points to plot
            double[,] polyPoints = this.calculatePoints(startPoint,endPoint,polyExpr,exprVar.ToString());

            // Plot the polynomial/function in its own thread otherwise would give error 
            // when I try to plot them in succession 
            // Code found online, StackOverflow, and I adapted it to what I need
            // Unfortunatly I cannot find the link of it anymore
            Thread newWindowThread = new Thread(new ThreadStart(() =>
            {
                // Create and show the Window
                PlotGraph plot = new PlotGraph(polyPoints);
                plot.Show();
                // Start the Dispatcher Processing
                System.Windows.Threading.Dispatcher.Run();
            }));
            // Set the apartment state
            newWindowThread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            newWindowThread.IsBackground = true;
            // Start the thread
            newWindowThread.Start();

            // Remove expression variable from the stored variable 
            GLOBAL_VAR.Remove(exprVar.ToString());
            return "PLOT " + polyName + ":" + polyExpr + " between [" + startPoint + "," + endPoint + "]";
        }

        /// <summary>
        /// Method to get the variable inside the polynomial/function
        /// </summary>
        /// <param name="polyExpr">The polynomial/function expression</param>
        /// <returns>The variable used in the polynomial/function</returns>
        private char getPolyVariable(string polyExpr)
        {
            char exprVar = ' ';
            int index = 0;
            bool isAlreadyDefined = false;
            // Scan the expression and check each char if is the varaible
            while (exprVar == ' ' && index < polyExpr.Length)
            {
                char c = polyExpr[index];
                if (Char.IsUpper(polyExpr[index]))
                {

                    exprVar = c;
                    isAlreadyDefined = false;
                    // test if expression variable is already a variable stored in the system 
                    string test = null;
                    try
                    {
                        test = GLOBAL_VAR[exprVar.ToString()];
                    }
                    catch (Exception e)
                    {
                        // DO NOTHING
                    }
                    if (test != null)
                    {
                        // There is already a variable associated with that ID
                        exprVar = ' ';
                        isAlreadyDefined = true;
                    }
                }
                index++;
            }
            // Throw exception if the variable of the polynomial/function is a variable stored in the system
            if (isAlreadyDefined)
            {
                throw new utilities.exceptions.VariableAlreadyDefinedExceptions();
            }
            // Throw exception if exprVal is still empty ' '
            if (exprVar == ' ')
            {
                throw new utilities.exceptions.NoPolynomialVariableFoundException(polyExpr);
            }
            return exprVar;
        }

        /// <summary>
        /// Method to calculate the point for plotting a polynomial/function
        /// </summary>
        /// <param name="startPoint">The starting point of the plot</param>
        /// <param name="endPoint">The end point of the plot</param>
        /// <param name="expression">The expression of the polynomial/function</param>
        /// <param name="variable">The variable of the polynomial/function</param>
        /// <returns></returns>
        private double[,] calculatePoints(int startPoint, int endPoint, string expression, string variable)
        {
            // calculate total number of points to be calculated
            // calculate a point each 0.5 and store them in an array 
            int difference = Math.Abs(startPoint - endPoint);
            int totalPoints = difference * 2 + 1;
            // At row 0 we are going to store the x point
            // At row 1 we are going to store the y result point
            double[,] points = new double[2, totalPoints];
            int index = 0;
            for (double i = startPoint; i <= endPoint; i += 0.5)
            {
                // create a varaible for expr variable
                string expr = variable + ":=" + i;
                Lexer l = new Lexer(expr);
                Parser p = new Parser(l);
                Interpreter interp = new Interpreter(p);
                // Store the variable 
                interp.interpret();

                l = new Lexer(expression);
                p = new Parser(l);
                interp = new Interpreter(p);
                // Get the result of the polynomial/function 
                var result = interp.interpret();

                points[0, index] = i;
                points[1, index] = result;
                index++;
            }
            return points;
        }

        
        /// <summary>
        /// Method to visit a MinMax node
        /// </summary>
        /// <param name="node">The MinMax node to be visited</param>
        /// <returns>The result of this MinMax</returns>
        private string visit_MinMax(AST.MinMax node)
        {
            // Get the poly name and its expression
            string polyName = node.getPolyVar();
            string polyExpr;
            try
            {
                polyExpr = GLOBAL_VAR[polyName];
            }
            catch (Exception e)
            {
                throw new utilities.exceptions.NoPolynomialFoundException(polyName);
            }
            // Check the degree of the polynomial 
            int degree = this.findPolyDegree(polyExpr);
            if (degree == 1)
            {
                return "Cannot find min or max of a linear degree polynomial : " + polyExpr;
            } else if (degree >= 3)
            {
                return "Not implemented yet for polynomials greater of the second degree";
            }
            // Find coefficient of the polynomilas 
            double[] polyCoefficients = findPolyCoefficient(polyExpr, degree);
            // Find the coefficient of the first derivative
            double[] firstDerCoeff = derivativeCoeff(polyCoefficients);
            // Find the coefficient of the second derivative
            double[] secondDerCoeff = derivativeCoeff(firstDerCoeff);
            // Create string for the result
            StringBuilder strRes = new StringBuilder();
            strRes.Append("Degree of the polynomial :" + degree + "\n");
            strRes.Append("Polynomial Coefficient : [ ");
            for (int i = 0; i < polyCoefficients.Length; i++)
            {
                strRes.Append(polyCoefficients[i] + " ");
            }
            strRes.Append("]\n");
            strRes.Append("First Derivative Coefficients : [ ");
            for (int i = 0; i < firstDerCoeff.Length; i++)
            {
                strRes.Append(firstDerCoeff[i] + " ");
            }
            strRes.Append("]\n");
            strRes.Append("Second Derivative Coefficients : [ ");
            for (int i = 0; i < secondDerCoeff.Length; i++)
            {
                strRes.Append(secondDerCoeff[i] + " ");
            }
            strRes.Append("]\n");
            // Test which degree the polynomial is and find min or max accordingly
            if (degree == 2)
            {
                // Find x results where there is a min or max
                // by solving the expression ,of the frist derivative,  ax + b = 0 
                // we can find the x point where there is the min or max by solving x = -b/a

                double x = -firstDerCoeff[1] / firstDerCoeff[0];

                // Find the variable of the polynomial 
                char exprVar = this.getPolyVariable(polyExpr);
                
                // now we solve the original polynomial using the x value found so that
                // we can find the y coordinate of the min or max
                string exprToSolve = polyCoefficients[0] + "*" + x + "^2+" + polyCoefficients[1] + "*" + x + "+" + polyCoefficients[2];
                Lexer l = new Lexer(exprToSolve);
                Parser p = new Parser(l);
                Interpreter interp = new Interpreter(p);
                var y = interp.interpret();
                // Check if is a min or max using the coefficinet of the second derivative
                if (secondDerCoeff[0] > 0)
                {
                    strRes.Append("There is a minimum at point [ " + x + "," + y + "]");

                } else
                {
                    strRes.Append("There is a maximum at point [ " + x + "," + y + "]");
                }
                // Calculate the start and end point around x value
                int startPoint = (int)x - 50;
                int endPoint = (int)x + 50;
                // Calculate point for polynomial 
                double[,] polyPoints = this.calculatePoints(startPoint, endPoint, polyExpr, exprVar.ToString());
                // Calcuate point for polynomial given the first derivative result
                string temp = polyExpr.Replace(exprVar.ToString(), x.ToString());
                double[,] derPoints = this.calculatePoints(startPoint, endPoint, temp, exprVar.ToString());
                // Plot both 
                Thread newWindowThread = new Thread(new ThreadStart(() =>
                {
                    PlotGraph test = new PlotGraph(polyPoints, derPoints);
                    test.Show();
                    // Start the Dispatcher Processing
                    System.Windows.Threading.Dispatcher.Run();
                }));
                // Set the apartment state
                newWindowThread.SetApartmentState(ApartmentState.STA);
                // Make the thread a background thread
                newWindowThread.IsBackground = true;
                // Start the thread
                newWindowThread.Start();
                // Remove the expression variable from the system
                GLOBAL_VAR.Remove(exprVar.ToString());
            }
            return strRes.ToString();
        }

        /// <summary>
        /// Method to get the degree of the polynomial
        /// </summary>
        /// <param name="poly"></param>
        /// <returns></returns>
        private int findPolyDegree(string poly)
        {

            int degree=-1;
            // variable used to check if polynomial is in the right format. 
            // it cound how many ^ there are which need to be the same as the degree of the polynomial. 
            // if not the polynomial is not in the correct format  
            int count = 0; 
            int index;
            while (( index = poly.IndexOf("^")) != -1)
            {
                // Access the degree and test if it is greater than the one found so far
                int newDegree = Int32.Parse(poly[index+1].ToString());  
                if (newDegree > degree)
                {
                    degree = newDegree;
                }
                // Remove ^ from poly so that we can find others
                poly = poly.Remove(index, 1);
                count++;
            }
            // Test if the given polynomial is in the correct format
            if (degree == -1 || degree!=count)
            {
                throw new utilities.exceptions.WrongPolyExpressionFormatForMinMax(poly);
            }
            return degree;
        }

        /// <summary>
        /// Find the coefficient of a polynomial
        /// </summary>
        /// <param name="poly">The expression of the polynomial</param>
        /// <param name="degree">The degree of the polynomial</param>
        /// <returns></returns>
        private double [] findPolyCoefficient(string poly,int degree)
        {
            // Create array where to store the coefficient
            double[] coefficient = new double[degree+1];
            // Use the + and - to split the polynomial expression
            char[] delimiters = new char[] { '+', '-' };
            string[] splitPoly = Regex.Split(poly, @"(?<=[+-])");
            // Iterate through each split and assign the coefficient to the right position in the array 
            for (int i= splitPoly.Length-1; i >=0 ;i--)
            {
                string temp = splitPoly[i];
                if (temp.Contains("^")) 
                {
                    // Get the degree of this section
                    int tempDegree = Int32.Parse(temp[(temp.IndexOf("^") + 1)].ToString());
                    // Remove everything after the * sign : a*X^n -> a
                    string test = temp.Remove(temp.IndexOf("*"));
                    // Check sing
                    char sign = '+';
                    // I need to check if i!=0 since I keep the split delimitor in order to get the sign 
                    // but if poly start with a -|+ then the first split term will be just that 
                    // and we do not need to check it
                    if (i != 0)
                    {
                        // Check sing of it and store it. Sign need to be taken from the previous split 
                        sign = splitPoly[i - 1][splitPoly[i - 1].Length - 1];
                        coefficient[degree - tempDegree] = Double.Parse(sign.ToString() + test);
                    }
                    else
                    {
                        coefficient[degree - tempDegree] = Double.Parse(sign.ToString() + test);
                    }
                }
                else
                {
                    // I need to check if i!=0 since I keep the split delimitor in order to get the sign 
                    // but if poly start with a -|+ then the first split term will be just that 
                    // and we do not need to check it
                    if (i != 0)
                    {
                        // Check sing of it and store it. Sign need to be taken from the previous split 
                        char sign = splitPoly[i - 1][splitPoly[i - 1].Length - 1];
                        coefficient[coefficient.Length - 1] = Double.Parse(sign.ToString() + temp);
                    }
                }
            }
            return coefficient;
        }

        /// <summary>
        /// Method to calculate the derivative coefficient given a valid array of coefficient
        /// </summary>
        /// <param name="polyCoeff">The coefficient from whom we calculate the derivative coefficient</param>
        /// <returns>The coefficient of the derivative</returns>
        private double [] derivativeCoeff(double [] polyCoeff)
        {
            // Create array where to store coefficient
            double[] derivativeCoeff = new double[polyCoeff.Length - 1];
            // Iterate through each coefficient and calculate the derivative coefficient 
            for (int i = 0; i < polyCoeff.Length-1; i++) 
            {
                derivativeCoeff[i] = polyCoeff[i] * (polyCoeff.Length - (i + 1));
            }
            return derivativeCoeff;
        }


        /// <summary>
        /// Method to visit a Variable node
        /// </summary>
        /// <param name="node">The Variable node to be visited</param>
        /// <returns>The number representation of the variable </returns>
        private double visit_Variable(AST.Variable node)
        {
            // Get variable name
            string varName = node.getTokenID().getStringValue();
            // Try to access variable value
            double value;
            try
            {
                value = double.Parse(GLOBAL_VAR[varName]);
            } catch (Exception e)
            {
                throw new utilities.exceptions.UnspecifiedVariableFound(varName);
            }
            return value;
        }
    }
}
